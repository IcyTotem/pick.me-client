<?xml version="1.0" encoding="utf-8"?>

<resources>
    <string name="app_name">pick.me</string>

    <string name="connecting">Connecting to server...</string>
    <string name="reconnecting">Trying to reconnect to server...</string>
    <string name="username">Username</string>
    <string name="confirm">Confirm</string>
    <string name="gender">Gender</string>
    <string name="birthday">Birthday</string>
    <string name="checking_username_availability">Checking username availability</string>
    <string name="age">Age</string>
    <string name="range">Range</string>
    <string name="edit">Edit</string>
    <string name="password">Password</string>
    <string name="confirm_password">Confirm password</string>
    <string name="country_code">Country code</string>
    <string name="creating_user">Creating new profile</string>
    <string name="logging_in">Loggin in...</string>
    <string name="brief_description">Brief description</string>
    <string name="select_picture">Select a picture</string>
    <string name="pick_description">Write a brief description</string>
    <string name="select_preferences">Select search preferences</string>
    <string name="send">Send</string>
    <string name="cancel">Cancel</string>
    <string name="accept">Accept</string>
    <string name="decline">Decline</string>
    <string name="you">You</string>
    <string name="blacklist">Blacklist</string>
    <string name="settings">Settings</string>
    <string name="logout">Logout</string>
    <string name="unfriend">Unfriend</string>
    <string name="quit">Quit</string>
    <string name="switch_device">Switch device</string>
    <string name="block">Block</string>
    <string name="change_password">Change password</string>
    <string name="refresh">Refresh</string>

    <string name="tab_nearby">Nearby</string>
    <string name="tab_requests">Requests</string>
    <string name="tab_friends">Friends</string>

    <string name="empty_list_nearby_users">No users nearby that match your search criteria</string>
    <string name="empty_list_requests">You have no pending requests</string>
    <string name="empty_list_friends">You have no friends :( So sad!</string>
    <string name="empty_list_messages">No messages to show</string>
    <string name="loading_list_nearby_users">Looking for nearby users...</string>
    <string name="loading_list_requests">Loading requests...</string>
    <string name="loading_list_friends">Loading friends list...</string>

    <string name="sort_by_range">Sort by distance</string>
    <string name="sort_by_last_update">Sort by last update</string>
    <string name="sort_by_unread_messages_count">Sort by messages</string>
    <string name="edit_profile">Edit profile</string>
    <string name="search_criteria">Search criteria</string>

    <string name="updated_x_ago" formatted="false">Updated %s ago</string>

    <string name="web_socket_service_foreground_description">You are connected to pick.me :)</string>

    <string name="welcome_message">Hi there and welcome to pick.me! What would you like to do?</string>
    <string name="start_menu_new">Create a new profile</string>
    <string name="start_menu_login">Login</string>
    <string name="start_menu_troubles">Troubleshooting</string>
    <string name="expon_create_profile_description">Choose an username, tell us something about you and you\'re good to go! No e-mail required, you can browse people in your vicinity right away!</string>
    <string name="expon_login_description">To start using pick.me you must first login using your username and password. After this, the app will login automatically for you :)</string>
    <string name="expon_troubleshooting_description">Want to change your password? Have you switched to a new device and your pick.me account is still bound to the old one? Has your profile been stolen? Solve all these issues here!</string>
    <string name="expon_quit_description">Closes the connection to pick.me (you won\'t receive any notification until your next login).</string>

    <string name="login_intro">To login, please insert your username and the password you chose during profile creation.</string>

    <string name="picture_overlay_tap_here">Tap here to insert a profile picture</string>
    <string name="take_photo">Take a photo</string>

    <string name="gather_profile_data_intro">Hi! Tell us something about you:</string>
    <string name="gather_user_preference_intro">Whom are you looking for?</string>
    <string name="create_profile_intro">Ok, let\'s start! We need some basic information:</string>
    <string name="edit_profile_data_intro">Edit your profile:</string>
    <string name="fill_profile_help_intro">Before starting, complete your profile with a description, a picture and your preferences!</string>
    <string name="troubleshooting_intro">Has something gone wrong? Here you can fix it!</string>

    <string name="expon_switch_device_description">Every pick.me account is bound to a device, but from time to time you may need to swap to a new device. Here you can tell us that the device you\'re using now must be coupled with your existing account.</string>
    <string name="expon_block_description">Is someone else using pick.me on your device without your consent? Here you can remotely block your account to prevent undesired behaviours.</string>
    <string name="expon_change_password_description">Here you can change your password: bear in mind that you can only do this if this device is currently associated to your pick.me acccount!</string>

    <string name="fill_profile_help_picture_ok">You have a profile picture!</string>
    <string name="fill_profile_help_description_ok">Nice catch-phrase!</string>
    <string name="fill_profile_help_preferences_ok">Search targets acquired!</string>

    <string name="send_request_dialog_title">Send a friend request</string>
    <string name="send_request_dialog_text" formatted="true">If the %s accepts your request, you\'ll be able to exchange text messages and view each other online. Please enter a message to be delivered together with your request:</string>
    <string name="request_sent">Request sent!</string>
    <string name="sending_request">Sending request...</string>
    <string name="request_accepted">Request accepted!</string>
    <string name="request_declined">Request declined!</string>
    <string name="request_received_notification_title">Request received!</string>
    <string name="request_received_notification_body" formatted="true">%s has sent you a request!</string>

    <string name="unfriended_notification_title">You have been unfriended!</string>
    <string name="unfriended_notification_body" formatted="true">%s removed you from their friend list!</string>
    <string name="blacklisted_notification_title">You have been blacklisted!</string>
    <string name="blacklisted_notification_body" formatted="false">%s has blacklisted you: \"%s\". You won\'t ever see each other again :(</string>

    <string name="request_says" formatted="true">%s says:</string>

    <string name="chat_message_header" formatted="false">[%s] %s wrote:</string>

    <string name="message_received_notification_title">You have a new message!</string>
    <string name="message_received_notification_body" formatted="false">%s: %s</string>

    <string name="notification_title">Welcome back to pick.me!</string>
    <string name="notification_body" formatted="false">You have %d new requests, %d new messages and %d new friends!</string>

    <string name="create_profile_gather_profile_data">Who are you?</string>
    <string name="create_profile_password_hint">For security reason, some operations will require you to enter a password to verify your identity. Please, select a suitable password (min 7 characters):</string>

    <string name="open_location_settings">Open location settings</string>
    <string name="error_title_no_provider_enabled">Geolocation provider missing</string>

    <string name="user_empty_description">New user</string>

    <string name="blacklisting_success_message" formatted="true">%s has been blacklisted!</string>
    <string name="blacklisting_dialog_title">Blacklisting</string>
    <string name="blacklisting_dialog_body" formatted="true">If you blacklist %s, you won\'t be able so see each other online or exchange text messages, forever. Are you sure? If yes, you may enter a brief motivation for your blacklisting:</string>

    <string name="friend_click_dialog_title">Unfriend / Blacklist</string>
    <string name="friend_click_dialog_body_f" formatted="true">Do you wish to remove %s from the list of your friends or to blacklist her?</string>
    <string name="friend_click_dialog_body_m" formatted="true">Do you wish to remove %s from the list of your friends or to blacklist him?</string>
    <string name="unfriend_success_message" formatted="true">%s is no longer your friend! So sad!</string>

    <string name="load_last_200_messages">Load last 200 messages</string>
    <string name="load_all_messages">Load all messages</string>
    <string name="fetch_messages">Fetch messages from server</string>
    <string name="fetching_messages">Fetching messages...</string>

    <string name="edit_settings_intro">Here you can tune the application settings:</string>
    <string name="preferred_location_provider">Preferred location provider</string>
    <string name="location_refresh_period">Location\nrefresh period</string>
    <string name="requests_pull_period">Requests\npull period</string>
    <string name="friends_pull_period">Friends\npull period</string>
    <string name="location_refresh_distance">Location\nrefresh distance</string>

    <string name="switch_device_description">Enter your username and password to confirm your identity. Once the process is done, you account will be bound to this device.</string>
    <string name="switch_device_success_message">Your pick.me account is now bound to this device. You can login normally.</string>
    <string name="block_description">Enter your username and password to confirm your identity. Blocking your account will kick anyone who\'s using it out of pick.me and unable to log back in.</string>
    <string name="block_success_message">The selected account has been blocked!</string>
    <string name="change_password_description">Before entering the new password, please confirm your identity by entering your current username and password.</string>
    <string name="change_password_success_message">Password changed successfully! You can login normally.</string>
    <string name="enter_new_password">Please enter your new password:</string>

    <string name="blocked_notification_title">You have been blocked!</string>
    <string name="blocked_notification_body">The owner of this account has blocked it and you cannot use it anymore!</string>

    <string name="request_accepted_notification_title">You have a new friend!</string>
    <string name="request_accepted_notification_body" formatted="true">%s accepted your request. Send them a message!</string>

    <string name="error_cannot_connect">Cannot connect to server. Retry later!</string>
    <string name="error_no_network">No internet connection is available!</string>
    <string name="error_saving_picture">An error occurred while saving your profile picture</string>
    <string name="error_username_empty">The username cannot be empty</string>
    <string name="error_username_length" formatted="false">Username must contain %d to %d characters</string>
    <string name="error_too_young" formatted="false">You must be at least %d years old to use this app</string>
    <string name="error_username_taken">This username is already in use</string>
    <string name="error_no_picture">No picture selected!</string>
    <string name="error_invalid_country_code">Country code must be two letter long</string>
    <string name="error_profile_data_needed">You need to fill in some basic information before creating your profile. Click on the button above!</string>
    <string name="error_password_mismatch">Passwords must match</string>
    <string name="error_password_too_short">Password too short</string>
    <string name="error_no_login_secret">No login data stored</string>
    <string name="error_no_provider_enabled">No location provider is enabled, therefore the application cannot work as intended. Please enable geolocation services! Please note that you will have to REBOOT your phone afterwards!!!</string>
    <string name="error_device_id_mismatch">This account is bound to a different device. To bind it to this device, please go to Troubleshooting in the Start Menu.</string>
    <string name="error_no_login_device_id">This account appears not to be bound to any device. How so?</string>
    <string name="error_login_service_missing">The login service isn\'t available! Try re-launching the app</string>
    <string name="error_request_text_empty">You must write something in your request, be creative!</string>
    <string name="error_chat_message_empty">Write something before sending the message!</string>
    <string name="error_chat_message_not_sent">An error occurred while sending your last message. Try again later.</string>
    <string name="error_fetching_messages">An error occurred while fetching messages.</string>
    <string name="error_network">A network error occurred: no response from server. Please logout and check your connection, then log back in!</string>

    <string name="ER_DUP_ENTRY">The database already contains this entry: for example, your username may be taken, or you already own an account on this phone.</string>
    <string name="ER_REQUEST_SELF">You cannot send a request to yorself!</string>
    <string name="ER_REQUEST_FRIEND">You cannot send a request to a friend!</string>
    <string name="ER_REQUEST_BLACKLISTED">You cannot send a request to someone that you have blacklisted or someone that blacklisted you!</string>
    <string name="ER_REQUEST_DECLINED">Your request has been declined and thus you cannot send it again so soon.</string>
    <string name="ER_INVALID_USER">Not a valid user</string>
    <string name="ER_INVALID_ID">Not a valid identifier</string>
    <string name="ER_REQUEST_INVALID_OWNER">Only tha target of a request can accept it.</string>
    <string name="ER_TOO_YOUNG">You are too young!</string>
    <string name="ER_WRONG_CREDENTIALS">Wrong username/password or device-id/secret.</string>
    <string name="ER_USER_BLOCKED">This account has been blocked by its owner.</string>
    <string name="ER_USERNAME_TAKEN">This username has already been taken!</string>
    <string name="ER_NO_CREDENTIALS">Cannot login without any credentials!</string>
    <string name="ER_REQUEST_ALREADY_ISSUED">You already sent a reuqest to this person</string>
    <string name="ER_ALREADY_LOGGED">There is already someone logged in with this account! Are you trying to log from a different device? Check Troubleshooting menu for more info!</string>
    <string name="ER_NO_ROWS_AFFECTED">No record was modified! Maybe you entered wrong data?</string>
    <string name="ECONNREFUSED">You forgot to start MySql server! What a fool!</string>
</resources>
