package org.pickme.app;

import android.content.Context;
import android.content.Intent;


public class Application {
    public static Intent getMainActivityIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    public static Intent getStartMenuActivityIntent(Context context) {
        Intent intent = new Intent(context, StartMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static Intent getStartActivityIntent(Context context) {
        Intent intent = new Intent(context, StartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static void startMainActivityFrom(Context context) {
        context.startActivity(getMainActivityIntent(context));
    }

    public static void startStartMenuActivityFrom(Context context) {
        context.startActivity(getStartMenuActivityIntent(context));
    }

    public static void startStartActivityFrom(Context context) {
        context.startActivity(getStartActivityIntent(context));
    }
}
