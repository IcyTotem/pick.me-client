package org.pickme.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.pickme.R;
import org.pickme.app.base.BaseActivity;
import org.pickme.service.LoginService;
import org.pickme.utils.Globals;
import org.pickme.entities.attributes.UserAttributes;

public class LoginActivity extends BaseActivity {
    public static final String INTENT_USE_RESULT = "use-result";
    public static final String INTENT_CUSTOM_DESCRIPTION = "custom-description";
    public static final String INTENT_CUSTOM_CONFIRM_TEXT = "custom-confirm-text";
    public static final String INTENT_DATA_USERNAME = "data-username";
    public static final String INTENT_DATA_PASSWORD = "data-password";

    private EditText txtName, txtPassword;
    private Button btnLogin;
    private TextView txtDescription;

    private boolean useResult;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.login);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        this.txtName = (EditText)findViewById(R.id.txtName);
        this.txtPassword = (EditText)findViewById(R.id.txtPassword);
        this.btnLogin = (Button)findViewById(R.id.btnLogin);
        this.txtDescription = (TextView)findViewById(R.id.txtDescription);

        this.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLoginOnClick(view);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        this.setControlsEnabled(true);
        this.useResult = false;

        Intent intent = this.getIntent();

        if (intent != null) {
            String customConfirmText = intent.getStringExtra(INTENT_CUSTOM_CONFIRM_TEXT);
            String customDescription = intent.getStringExtra(INTENT_CUSTOM_DESCRIPTION);

            if ((customConfirmText != null) && (customConfirmText.length() != 0))
                this.btnLogin.setText(customConfirmText);

            if ((customDescription != null) && (customDescription.length() != 0))
                this.txtDescription.setText(customDescription);

            this.useResult = intent.getBooleanExtra(INTENT_USE_RESULT, false);
        }

        this.setupLoginCallbacks();
    }

    private void setupLoginCallbacks() {
        final LoginService loginService = LoginService.getInstance();

        // When this activity is visible, the following are the callbacks invoked when login succeeds/fails
        loginService.runAfterLogin(new Runnable() {
            @Override
            public void run() {
                SharedPreferences prefs = UserAttributes.get(getApplicationContext());

                if (prefs.getBoolean(UserAttributes.FIRST_LOGIN, true))
                    startActivity(new Intent(getApplicationContext(), FillProfileHelpActivity.class));
                else
                    Application.startMainActivityFrom(LoginActivity.this);

                // Ends the calling activity, i.e. this one
                finish();
            }
        });

        loginService.runOnLoginFailure(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showToast(loginService.getLastError());
                        btnLogin.setText(getResources().getText(R.string.start_menu_login));
                        setControlsEnabled(true);
                    }
                });
            }
        });
    }


    private void btnLoginOnClick(View view) {
        String username = txtName.getText().toString();
        String password = txtPassword.getText().toString();

        if ((username.length() < Globals.MIN_USERNAME_LENGTH) || (username.length() > Globals.MAX_USERNAME_LENGTH)) {
            this.showToast(R.string.error_username_length, Globals.MIN_USERNAME_LENGTH, Globals.MAX_USERNAME_LENGTH);
            return;
        }

        if (password.length() < Globals.MIN_PASSWORD_LENGTH) {
            this.showToast(R.string.error_password_too_short);
            return;
        }

        if (useResult) {
            Intent intentData = new Intent();
            intentData.putExtra(INTENT_DATA_USERNAME, username);
            intentData.putExtra(INTENT_DATA_PASSWORD, password);

            this.setResult(RESULT_OK, intentData);
            this.finish();
        } else {
            if (!LoginService.isAvailable()) {
                this.showToast(R.string.error_login_service_missing);
                return;
            }

            this.setControlsEnabled(false);
            this.btnLogin.setText(getResources().getText(R.string.logging_in));

            LoginService loginService = LoginService.getInstance();
            loginService.loginManually(username, password);
        }
    }
}