package org.pickme.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import org.pickme.R;
import org.pickme.app.base.BaseActivity;
import org.pickme.app.dialogs.GatherProfileDataActivity;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.utils.Globals;
import org.pickme.utils.ToastHelper;
import org.pickme.entities.attributes.UserAttributes;
import org.pickme.utils.Utils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class CreateProfileActivity extends BaseActivity {
    private static final int GATHER_PROFILE_DATA = 1;
    private static final int EDIT_PROFILE_DATA = 2;

    private Button btnGatherProfileData, btnConfirm;
    private ImageButton btnGatherProfileDataInline;
    private TextView txtProfileData;
    private View layoutGatheredProfileData;
    private EditText txtPassword, txtConfirmPassword;

    private Profile profile;
    private boolean creatingUser = false;

    private boolean isProfileComplete() {
        return (profile.getName() != null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.create_profile);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        this.hookWebSocketService();

        this.btnGatherProfileData = (Button)findViewById(R.id.btnGatherProfileData);
        this.btnGatherProfileDataInline = (ImageButton)findViewById(R.id.btnGatherProfileDataInline);
        this.txtProfileData = (TextView)findViewById(R.id.txtProfileData);
        this.layoutGatheredProfileData = findViewById(R.id.layoutGatheredProfileData);
        this.txtPassword = (EditText)findViewById(R.id.txtPassword);
        this.txtConfirmPassword = (EditText)findViewById(R.id.txtConfirmPassword);
        this.btnConfirm = (Button)findViewById(R.id.btnConfirm);

        this.initProfile();
        this.setupTextWatchers();

        this.btnGatherProfileData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GatherProfileDataActivity.class);
                startActivityForResult(intent, GATHER_PROFILE_DATA);
            }
        });
        this.btnGatherProfileDataInline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GatherProfileDataActivity.class);
                startActivityForResult(intent, EDIT_PROFILE_DATA);
            }
        });
        this.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnConfirmOnClick(view);
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        this.saveProfile();
    }

    @Override
    public void onStart() {
        super.onStart();

        this.loadProfile();

        if (this.isProfileComplete()) {
            btnGatherProfileData.setVisibility(View.GONE);
            layoutGatheredProfileData.setVisibility(View.VISIBLE);

            try {
                Date birthdayDate = Globals.DATE_FORMAT.parse(profile.getBirthday());
                Calendar birthdayCalendar = Calendar.getInstance();
                birthdayCalendar.setTime(birthdayDate);
                txtProfileData.setText(String.format("%s, %s (%d)", profile.getName(), profile.getCountry(), Utils.computeAge(birthdayCalendar)));
            } catch (ParseException e) {  }
        }

        this.setControlsEnabled(!creatingUser);

        // Only enables confirm button is password fields are properly filled
        String password = txtPassword.getText().toString();
        String confirmPassword = txtConfirmPassword.getText().toString();
        boolean ok = password.equals(confirmPassword) && (password.length() >= Globals.MIN_PASSWORD_LENGTH);
        btnConfirm.setEnabled(ok);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.service.unregisterCallbacks(WebSocket.Messages.CREATE_USER);
    }


    private void loadProfile() {
        TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        SharedPreferences preferences = this.getPreferences(MODE_PRIVATE);

        profile.setName(preferences.getString(PREF_USERNAME, null));
        profile.setBirthday(preferences.getString(PREF_BIRTHDAY, null));
        profile.setGender(preferences.getString(PREF_GENDER, null));
        profile.setCountry(preferences.getString(PREF_COUNTRY, null));
        profile.setPassword(preferences.getString(PREF_PASSWORD, null));
        profile.setDeviceID(preferences.getString(PREF_DEVICE_ID, deviceID));
    }

    private void saveProfile() {
        SharedPreferences preferences = this.getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREF_USERNAME, profile.getName());
        editor.putString(PREF_DEVICE_ID, profile.getDeviceID());
        editor.putString(PREF_BIRTHDAY, profile.getBirthday());
        editor.putString(PREF_GENDER, profile.getGender());
        editor.putString(PREF_COUNTRY, profile.getCountry());
        editor.putString(PREF_PASSWORD, profile.getPassword());

        editor.apply();
    }

    private void setupTextWatchers() {
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String password = txtPassword.getText().toString();
                String confirmPassword = txtConfirmPassword.getText().toString();
                boolean ok = password.equals(confirmPassword) && (password.length() >= Globals.MIN_PASSWORD_LENGTH);

                btnConfirm.setEnabled(ok);

                if (ok) {
                    profile.setPassword(password);
                    btnConfirm.setText(getResources().getString(R.string.confirm));
                } else
                    if (!password.equals(confirmPassword))
                        btnConfirm.setText(getResources().getString(R.string.error_password_mismatch));
                    else
                        btnConfirm.setText(getResources().getString(R.string.error_password_too_short));
            }
        };

        this.txtPassword.addTextChangedListener(watcher);
        this.txtConfirmPassword.addTextChangedListener(watcher);
    }

    private void initProfile() {
        TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();

        this.profile = new Profile();
        this.profile.setDeviceID(deviceID);
    }


    @Override
    protected void setupWebSocketHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                final String secret = args[0].toString();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onUserCreationSuccess(secret);
                    }
                });
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                final String error = args[0].toString();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onUserCreationFailure(error);
                    }
                });
            }
        };

        this.service.registerCallbacks(WebSocket.Messages.CREATE_USER, successHandler, failureHandler);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode != GATHER_PROFILE_DATA) && (requestCode != EDIT_PROFILE_DATA))
            return;

        if (resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            profile.setName(extras.getString(GatherProfileDataActivity.INTENT_DATA_USERNAME));
            profile.setCountry(extras.getString(GatherProfileDataActivity.INTENT_DATA_COUNTRY_CODE));
            profile.setGender(extras.getString(GatherProfileDataActivity.INTENT_DATA_GENDER));

            Calendar birthday = (Calendar)extras.getSerializable(GatherProfileDataActivity.INTENT_DATA_BIRTHDAY);
            profile.setBirthday(Globals.DATE_FORMAT.format(birthday.getTime()));

            txtProfileData.setText(String.format("%s, %s (%d)", profile.getName(), profile.getCountry(), Utils.computeAge(birthday)));

            if (requestCode == GATHER_PROFILE_DATA) {
                btnGatherProfileData.setVisibility(View.GONE);
                layoutGatheredProfileData.setVisibility(View.VISIBLE);
            }

            this.saveProfile();
        }
    }


    private void btnConfirmOnClick(View view) {
        if (!this.isProfileComplete()) {
            this.showToast(R.string.error_profile_data_needed);
            return;
        }

        this.creatingUser = true;
        this.setControlsEnabled(false);

        btnConfirm.setText(getResources().getString(R.string.creating_user));

        webSocket.emit(WebSocket.Messages.CREATE_USER, profile);
    }


    private void onUserCreationFailure(String error) {
        this.creatingUser = false;
        this.setControlsEnabled(true);
        ToastHelper.showToastInUiThread(this, error);
    }

    private void onUserCreationSuccess(String secret) {
        this.creatingUser = false;
        this.setControlsEnabled(true);

        SharedPreferences.Editor editor = UserAttributes.edit(this);

        editor.putString(UserAttributes.LOGIN_SECRET, secret);
        editor.putString(UserAttributes.NAME, profile.getName());
        editor.putString(UserAttributes.GENDER, profile.getGender());
        editor.putString(UserAttributes.BIRTHDAY, profile.getBirthday());
        editor.putString(UserAttributes.COUNTRY, profile.getCountry());
        editor.putBoolean(UserAttributes.FIRST_LOGIN, true);
        editor.apply();

        Application.startStartActivityFrom(this);
    }


    private static final String PREF_USERNAME = "username";
    private static final String PREF_DEVICE_ID = "device-id";
    private static final String PREF_PASSWORD = "password";
    private static final String PREF_GENDER = "gender";
    private static final String PREF_BIRTHDAY = "birthday";
    private static final String PREF_COUNTRY = "country";

    private static class Profile {
        private String name, deviceID, password, gender, birthday, country;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }
    }
}