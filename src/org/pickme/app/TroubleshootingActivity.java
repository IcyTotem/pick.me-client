package org.pickme.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import org.pickme.R;
import org.pickme.app.base.BaseExponActivity;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.utils.Globals;
import org.pickme.utils.ToastHelper;

import java.util.ArrayList;
import java.util.List;

public class TroubleshootingActivity extends BaseExponActivity {
    private static final int SWITCH_DEVICE = 1;
    private static final int BLOCK = 2;
    private static final int CHANGE_PASSWORD = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.troubleshooting);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        this.hookWebSocketService();

        final List<LinearLayout> expons = new ArrayList<LinearLayout>();

        this.addExpon(R.id.exponSwitchDevice);
        this.addExpon(R.id.exponBlock);
        this.addExpon(R.id.exponChangePassword);
    }

    @Override
    protected void setupWebSocketHandlers() {
        this.setupSwitchDeviceHandlers();
        this.setupBlockHandlers();
        this.setupChangePasswordHandlers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        service.unregisterCallbacks(WebSocket.Messages.SWITCH_DEVICE);
        service.unregisterCallbacks(WebSocket.Messages.BLOCK);
        service.unregisterCallbacks(WebSocket.Messages.CHANGE_PASSWORD);
    }

    private void setupSwitchDeviceHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                enableExponsInUiThread();
                ToastHelper.showToastInUiThread(TroubleshootingActivity.this, R.string.switch_device_success_message);
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String error = args[0].toString();
                enableExponsInUiThread();
                ToastHelper.showToastInUiThread(TroubleshootingActivity.this, error);
            }
        };

        service.registerCallbacks(WebSocket.Messages.SWITCH_DEVICE, successHandler, failureHandler);
    }

    private void setupBlockHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                enableExponsInUiThread();
                ToastHelper.showToastInUiThread(TroubleshootingActivity.this, R.string.block_success_message);
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String error = args[0].toString();
                enableExponsInUiThread();
                ToastHelper.showToastInUiThread(TroubleshootingActivity.this, error);
            }
        };

        service.registerCallbacks(WebSocket.Messages.BLOCK, successHandler, failureHandler);
    }

    private void setupChangePasswordHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                enableExponsInUiThread();
                ToastHelper.showToastInUiThread(TroubleshootingActivity.this, R.string.change_password_success_message);
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String error = args[0].toString();
                enableExponsInUiThread();
                ToastHelper.showToastInUiThread(TroubleshootingActivity.this, error);
            }
        };

        service.registerCallbacks(WebSocket.Messages.CHANGE_PASSWORD, successHandler, failureHandler);
    }

    private void enableExponsInUiThread() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setExponsEnabled(true);
            }
        });
    }


    @Override
    public void onExponClicked(View view) {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        Integer requestCode = -1;

        intent.putExtra(LoginActivity.INTENT_USE_RESULT, true);

        switch (view.getId()) {
            case R.id.exponSwitchDevice:
                requestCode = SWITCH_DEVICE;
                intent.putExtra(LoginActivity.INTENT_CUSTOM_CONFIRM_TEXT, getString(R.string.switch_device));
                intent.putExtra(LoginActivity.INTENT_CUSTOM_DESCRIPTION, getString(R.string.switch_device_description));
                break;

            case R.id.exponBlock:
                requestCode = BLOCK;
                intent.putExtra(LoginActivity.INTENT_CUSTOM_CONFIRM_TEXT, getString(R.string.block));
                intent.putExtra(LoginActivity.INTENT_CUSTOM_DESCRIPTION, getString(R.string.block_description));
                break;

            case R.id.exponChangePassword:
                requestCode = CHANGE_PASSWORD;
                intent.putExtra(LoginActivity.INTENT_CUSTOM_CONFIRM_TEXT, getString(R.string.change_password));
                intent.putExtra(LoginActivity.INTENT_CUSTOM_DESCRIPTION, getString(R.string.change_password_description));
                break;
        }

        startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK)
            return;

        final String username = data.getStringExtra(LoginActivity.INTENT_DATA_USERNAME);
        final String password = data.getStringExtra(LoginActivity.INTENT_DATA_PASSWORD);

        switch (requestCode) {
            case SWITCH_DEVICE:
                TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
                String deviceID = telephonyManager.getDeviceId();
                setExponsEnabled(false);
                webSocket.emit(WebSocket.Messages.SWITCH_DEVICE, username, password, deviceID);
                break;

            case BLOCK:
                setExponsEnabled(false);
                webSocket.emit(WebSocket.Messages.BLOCK, username, password);
                break;

            case CHANGE_PASSWORD:
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle(getString(R.string.change_password));
                builder.setMessage(getString(R.string.enter_new_password));

                final EditText input = new EditText(this);
                input.setSingleLine();
                input.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);

                builder.setView(input);

                builder.setNegativeButton(R.string.cancel, null);
                builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String newPassword = input.getText().toString();
                        if (newPassword.length() < Globals.MIN_PASSWORD_LENGTH)
                            showToast(R.string.error_password_too_short);
                        else {
                            setExponsEnabled(false);
                            webSocket.emit(WebSocket.Messages.CHANGE_PASSWORD, username, password, newPassword);
                        }
                    }
                });

                Dialog dialog = builder.create();
                dialog.show();
                break;
        }
    }
}