package org.pickme.app.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pickme.R;
import org.pickme.app.adapters.MessageAdapter;
import org.pickme.entities.Friend;
import org.pickme.entities.Message;
import org.pickme.service.MessageHistoryService;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.service.WebSocketService;
import org.pickme.utils.Globals;
import org.pickme.entities.attributes.MessageAttributes;

import java.util.*;

public class MessagesListFragment extends ListFragment {
    private MessageHistoryService messageHistoryService;
    private WebSocketService webSocketService;
    private WebSocket webSocket;
    private WebSocketEventHandler messageReceivedHandler, messageReadHandler, allMessagesReadByHandler;

    private Friend target;
    private List<Message> messages;
    private MessageAdapter adapter;
    private boolean viewCreated;


    public Friend getTarget() {
        return target;
    }

    public void setTarget(Friend target) {
        this.target = target;
        this.loadRecentMessages(Globals.DEFAULT_RECENT_MESSAGES_COUNT);

        webSocket.emit(WebSocket.Messages.GET_UNREAD_MESSAGES_FROM, target.getID());
    }


    public MessagesListFragment() {
        this.viewCreated = false;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.messages = new ArrayList<Message>();
        this.adapter = new MessageAdapter(getActivity(), messages);
        this.setListAdapter(adapter);
        this.notifyUiDataSetChanged();

        this.messageHistoryService = MessageHistoryService.getInstance();
        this.webSocketService = WebSocketService.getInstance();
        this.webSocket = webSocketService.getWebSocket();
        this.setupWebSocketHandlers();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!webSocketService.hasHandler(WebSocket.Messages.MESSAGE_RECEIVED, messageReceivedHandler))
            this.setupMessageReceivedHandler();

        if (!webSocketService.hasHandler(WebSocket.Messages.MESSAGE_READ, messageReadHandler))
            this.setupMessageReadHandler();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.GET_ALL_MESSAGES_FROM))
            this.setupGetAllMessagesFromHandlers();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.GET_UNREAD_MESSAGES_FROM))
            this.setupGetUnreadMessagesFromHandlers();

        if (!webSocketService.hasHandler(WebSocket.Messages.ALL_MESSAGES_READ_BY, allMessagesReadByHandler))
            this.setupAllMessagesReadByHandler();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        viewCreated = false;

        webSocketService.unregisterHandler(WebSocket.Messages.MESSAGE_RECEIVED, messageReceivedHandler);
        webSocketService.unregisterHandler(WebSocket.Messages.MESSAGE_READ, messageReadHandler);
        webSocketService.unregisterCallbacks(WebSocket.Messages.GET_ALL_MESSAGES_FROM);
        webSocketService.unregisterHandler(WebSocket.Messages.ALL_MESSAGES_READ_BY, allMessagesReadByHandler);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        viewCreated = true;
        return super.onCreateView(inflater, container, bundle);
    }

    private void setupWebSocketHandlers() {
        this.setupMessageReceivedHandler();
        this.setupMessageReadHandler();
        this.setupGetAllMessagesFromHandlers();
        this.setupGetUnreadMessagesFromHandlers();
        this.setupAllMessagesReadByHandler();
    }

    private void setupMessageReceivedHandler() {
        this.messageReceivedHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                JSONObject messageObject = (JSONObject)args[0];
                onMessageReceived(messageObject);
            }
        };

        webSocketService.registerHandler(WebSocket.Messages.MESSAGE_RECEIVED, messageReceivedHandler);
    }

    private void setupMessageReadHandler() {
        this.messageReadHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                int messageID = Integer.parseInt(args[0].toString());
                onMessageRead(messageID);
            }
        };

        webSocketService.registerHandler(WebSocket.Messages.MESSAGE_READ, messageReadHandler);
    }

    private void setupGetAllMessagesFromHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                JSONArray messagesArray = (JSONArray) args[0];
                onGetAllMessagesFromSuccess(messagesArray);
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setEmptyText(getString(R.string.error_fetching_messages));
                    }
                });
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.GET_ALL_MESSAGES_FROM, successHandler, failureHandler);
    }

    private void setupGetUnreadMessagesFromHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                try {
                    JSONArray argumentArray = (JSONArray) args[0];
                    JSONArray messagesArray = argumentArray.getJSONArray(0);
                    onGetUnreadMessagesFromSuccess(messagesArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setEmptyText(getString(R.string.error_fetching_messages));
                    }
                });
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.GET_UNREAD_MESSAGES_FROM, successHandler, failureHandler);
    }

    private void setupAllMessagesReadByHandler() {
        this.allMessagesReadByHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                Integer targetUserID = Integer.parseInt(args[0].toString());
                onAllMessagesReadBy(targetUserID);
            }
        };

        webSocketService.registerHandler(WebSocket.Messages.ALL_MESSAGES_READ_BY, allMessagesReadByHandler);
    }


    public void loadRecentMessages(int count) {
        if ((target == null) || (count == 0))
            return;

        List<Message> previousMessages = messageHistoryService.retrieveHistory(target.getID());

        for (Message message : previousMessages)
            if (!message.isMine())
                message.setSender(target);

        synchronized (this.messages) {
            this.messages.clear();

            if ((count == -1) || (previousMessages.size() < count))
                this.messages.addAll(previousMessages);
            else
                this.messages.addAll(previousMessages.subList(
                        previousMessages.size() - count,
                        previousMessages.size()
                ));

            this.notifyUiDataSetChanged();
        }
    }

    public void loadAllMessages() {
        this.loadRecentMessages(-1);
    }

    public void fetchMessages() {
        if (target == null)
            return;

        this.setEmptyText(getString(R.string.fetching_messages));

        this.messages.clear();
        this.notifyUiDataSetChanged();

        webSocket.emit(WebSocket.Messages.GET_ALL_MESSAGES_FROM, target.getID());
    }


    public Message addMyMessage(String text) {
        Message message = new Message();
        message.setMine(true);
        message.setSendDate(Calendar.getInstance());
        message.setText(text);

        messageHistoryService.addMyMessageToHistory(message, target.getID());

        this.messages.add(message);
        this.notifyUiDataSetChanged();

        return message;
    }

    public void removeMyLastMessages() {
        for (int i = messages.size() - 1; i >= 0; i--) {
            if (messages.get(i).isMine())
                messages.remove(i);
            else
                break;
        }

        this.notifyUiDataSetChanged();
    }
    
    public void setMyMessageID(int hashCode, int messageID) {
        for (Message message : messages)
            if (message.hashCode() == hashCode) {
                message.setID(messageID);
                break;
            }
    }


    private void onMessageReceived(JSONObject messageObject) {
        Message message = new Message();

        try {
            int senderID = messageObject.getInt(MessageAttributes.SENDER_USER_ID);

            // Message from another conversation: another service will handle that
            if (senderID != this.getTarget().getID())
                return;

            message.setID(messageObject.getInt(MessageAttributes.ID));
            message.setSendDate(messageObject.getString(MessageAttributes.SENT_DATE));
            message.setText(messageObject.getString(MessageAttributes.TEXT));
        } catch (JSONException e) {
            return;
        }

        message.setSender(this.getTarget());
        message.setRead(true);

        this.messages.add(message);
        this.notifyUiDataSetChanged();

        webSocket.emit(WebSocket.Messages.MARK_MESSAGE_AS_READ, message.getID());
    }

    private void onMessageRead(int messageID) {
        for (Message message : messages)
            if (message.getID() == messageID) {
                message.setRead(true);
                break;
            }

        this.notifyUiDataSetChanged();
    }

    private void onGetAllMessagesFromSuccess(JSONArray messagesArray) {
        List<Message> allMessages = parseJsonMessages(messagesArray);

        messageHistoryService.overrideHistory(allMessages, target.getID());

        if (allMessages.size() == 0)
            this.getListView().post(new Runnable() {
                @Override
                public void run() {
                    setEmptyText(getString(R.string.empty_list_messages));
                }
            });

        this.messages.addAll(allMessages);
        this.notifyUiDataSetChanged();
    }

    private void onGetUnreadMessagesFromSuccess(JSONArray messagesArray) {
        List<Message> unreadMessages = parseJsonMessages(messagesArray);

        synchronized (this.messages) {
            List<Message> notYetSavedMessages = new ArrayList<Message>();

            // By construction, messages are retrieved in the order they are sent and therefore in increasing
            // order of their ID value
            for (Message message : unreadMessages)
                if (Collections.binarySearch(this.messages, message, idMessageComparator) < 0)
                    notYetSavedMessages.add(message);

            this.messages.addAll(notYetSavedMessages);
            this.notifyUiDataSetChanged();

            messageHistoryService.addToHistory(notYetSavedMessages, target.getID());
        }

        webSocket.emit(WebSocket.Messages.READ_MESSAGES_FROM, target.getID());
    }

    private void onAllMessagesReadBy(int targetUserID) {
        if ((target == null) || (targetUserID != target.getID()))
            return;

        synchronized (this.messages) {
            for (Message message : messages)
                message.setRead(true);

            this.notifyUiDataSetChanged();
        }
    }


    private List<Message> parseJsonMessages(JSONArray messagesArray) {
        List<Message> result = new ArrayList<Message>();

        for (int i = 0; i < messagesArray.length(); i++) {
            try {
                JSONObject messageObject = messagesArray.getJSONObject(i);
                Integer senderID = messageObject.getInt(MessageAttributes.SENDER_USER_ID);
                Message message = new Message();

                message.setID(messageObject.getInt(MessageAttributes.ID));
                message.setSendDate(messageObject.getString(MessageAttributes.SENT_DATE));
                message.setText(messageObject.getString(MessageAttributes.TEXT));
                message.setRead(messageObject.getInt(MessageAttributes.READ) != 0);

                if (senderID == target.getID())
                    message.setSender(target);
                else
                    message.setMine(true);

                result.add(message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }


    private void notifyUiDataSetChanged() {
        FragmentActivity activity = getActivity();

        if ((activity == null) || (adapter == null))
            return;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isAdded() && viewCreated) {
                    adapter.notifyDataSetChanged();
                    setSelection(adapter.getCount() - 1);
                }
            }
        });
    }


    private static Comparator<Message> idMessageComparator = new IDMessageComparator();

    private static class IDMessageComparator implements Comparator<Message> {
        @Override
        public int compare(Message m0, Message m1) {
            return m0.getID() - m1.getID();
        }

        @Override
        public boolean equals(Object o) {
            return false;
        }
    }
}
