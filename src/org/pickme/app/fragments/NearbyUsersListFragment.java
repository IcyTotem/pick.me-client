package org.pickme.app.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pickme.R;
import org.pickme.app.adapters.UserAdapter;
import org.pickme.entities.User;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.utils.ToastHelper;
import org.pickme.entities.attributes.UserAttributes;
import org.pickme.utils.UserSettings;

import java.util.ArrayList;
import java.util.List;

public class NearbyUsersListFragment extends UserListFragment<User> {
    private static final String TAG = NearbyUsersListFragment.class.getSimpleName();
    private static final String STATE_SORTING = "sorting";

    private Sorting sorting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.GET_NEARBY_USERS))
            this.setupGetNearbyUsersHandlers();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.SEND_REQUEST))
            this.setupSendRequestHandlers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        webSocketService.unregisterCallbacks(WebSocket.Messages.GET_NEARBY_USERS);
        webSocketService.unregisterCallbacks(WebSocket.Messages.SEND_REQUEST);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(STATE_SORTING, sorting.ordinal());
    }

    @Override
    public Bundle onRestoreInstanceState(Bundle savedInstanceState) {
        Bundle bundle = super.onRestoreInstanceState(savedInstanceState);

        if (bundle != null)
            this.sorting = Sorting.values()[bundle.getInt(STATE_SORTING)];
        else
            this.sorting = Sorting.NONE;

        if (sorting.equals(Sorting.BY_RANGE))
            this.sortByRange();
        else if (sorting.equals(Sorting.BY_LAST_UPDATE))
            this.sortByLastUpdate();

        if (this.getUsers().size() == 0)
            this.setEmptyText(R.string.empty_list_nearby_users);

        return bundle;
    }


    @Override
    protected Long getUpdatePeriod() {
        SharedPreferences prefs = UserSettings.get(getActivity());
        return prefs.getLong(UserSettings.LOCATION_UPDATE_PERIOD, UserSettings.DEFAULT_LOCATION_UPDATE_PERIOD);
    }

    @Override
    protected void requireWebUpdate() {
        webSocket.emit(WebSocket.Messages.GET_NEARBY_USERS);
        setEmptyText(R.string.loading_list_nearby_users);
    }

    @Override
    protected void setupWebSocketHandlers() {
        super.setupWebSocketHandlers();

        this.setupGetNearbyUsersHandlers();
        this.setupSendRequestHandlers();
    }

    private void setupGetNearbyUsersHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                try {
                    JSONArray dataArray = (JSONArray) args[0];
                    JSONArray usersArray = dataArray.getJSONArray(0);
                    onGetNearbyUsersSuccess(usersArray);
                } catch (JSONException e) { }
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                Log.i(TAG, "Nearby users pull failed");
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.GET_NEARBY_USERS, successHandler, failureHandler);
    }

    private void setupSendRequestHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastHelper.showToastInUiThread(getActivity(), R.string.request_sent);
                    }
                });
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                final String error = args[0].toString();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastHelper.showToastInUiThread(getActivity(), error);
                    }
                });
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.SEND_REQUEST, successHandler, failureHandler);
    }


    private void onGetNearbyUsersSuccess(JSONArray usersArray) {
        List<User> newUsers = new ArrayList<User>();

        for (int i = 0; i < usersArray.length(); i++) {
            try {
                JSONObject userObject = usersArray.getJSONObject(i);
                User user = new User();

                user.setID(userObject.getInt(UserAttributes.ID));
                user.setName(userObject.getString(UserAttributes.NAME));
                user.setRange(userObject.getInt(UserAttributes.RANGE));
                user.setDescription(userObject.getString(UserAttributes.DESCRIPTION));
                user.setGender(userObject.getString(UserAttributes.GENDER));
                user.setBirthday(userObject.getString(UserAttributes.BIRTHDAY));
                user.setLastUpdate(userObject.getString(UserAttributes.LAST_UPDATE));
                user.setPictureFile(getActivity());

                newUsers.add(user);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ArrayList<User> users = super.getUsers();
        users.clear();
        users.addAll(newUsers);

        setEmptyText(R.string.empty_list_nearby_users);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (sorting.equals(Sorting.BY_RANGE))
                    sortByRange();
                else if (sorting.equals(Sorting.BY_LAST_UPDATE))
                    sortByLastUpdate();
                else // sort methods automatically call notifyDataSetChanged
                    getUserAdapter().notifyDataSetChanged();
            }
        });
    }


    public void sortByRange() {
        UserAdapter adapter = super.getUserAdapter();

        this.sorting = Sorting.BY_RANGE;

        adapter.sort(User.RANGE_COMPARATOR);
        adapter.notifyDataSetChanged();
    }

    public void sortByLastUpdate() {
        UserAdapter adapter = super.getUserAdapter();

        this.sorting = Sorting.BY_LAST_UPDATE;

        adapter.sort(User.LAST_UPDATE_COMPARATOR);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onListItemClick(ListView parent, View view, int position, long id) {
        super.onListItemClick(parent, view, position, id);

        final User user = super.getUsers().get(position);
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

        alert.setTitle(getString(R.string.send_request_dialog_title));
        alert.setMessage(getString(R.string.send_request_dialog_text, user.getName()));

        final EditText input = new EditText(getActivity());
        input.setSingleLine();
        input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        alert.setView(input);

        alert.setNegativeButton(getString(R.string.cancel), null);
        alert.setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();

                if (value.length() == 0) {
                    ToastHelper.showToast(getActivity(), R.string.error_request_text_empty);
                    return;
                }

                Request request = new Request();
                request.setTargetID(user.getID());
                request.setText(value);

                webSocket.emit(WebSocket.Messages.SEND_REQUEST, request);
            }
        });

        alert.show();
    }


    private enum Sorting {
        NONE,
        BY_RANGE,
        BY_LAST_UPDATE
    }

    private static class Request {
        private int targetID;
        private String text;

        public int getTargetID() {
            return targetID;
        }

        public void setTargetID(int id) {
            this.targetID = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}