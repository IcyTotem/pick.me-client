package org.pickme.app.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pickme.R;
import org.pickme.entities.RequestingUser;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.entities.attributes.RequestingUserAttributes;
import org.pickme.utils.ToastHelper;
import org.pickme.entities.attributes.UserAttributes;
import org.pickme.utils.UserSettings;

import java.util.ArrayList;
import java.util.List;

public class RequestsListFragment extends UserListFragment<RequestingUser> {
    private static final String TAG = RequestsListFragment.class.getSimpleName();

    private WebSocketEventHandler requestReceivedHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.GET_INCOMING_REQUESTS))
            this.setupGetIncomingRequestsHandlers();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.ACCEPT_REQUEST))
            this.setupAcceptRequestHandlers();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.DECLINE_REQUEST))
            this.setupDeclineRequestHandlers();

        if (!webSocketService.hasHandler(WebSocket.Messages.REQUEST_RECEIVED, requestReceivedHandler))
            this.setupRequestReceivedHandler();
    }

    @Override
    public Bundle onRestoreInstanceState(Bundle savedInstanceState) {
        Bundle result = super.onRestoreInstanceState(savedInstanceState);

        if (this.getUsers().size() == 0)
            this.setEmptyText(R.string.empty_list_requests);

        return result;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        webSocketService.unregisterCallbacks(WebSocket.Messages.GET_INCOMING_REQUESTS);
        webSocketService.unregisterCallbacks(WebSocket.Messages.ACCEPT_REQUEST);
        webSocketService.unregisterCallbacks(WebSocket.Messages.DECLINE_REQUEST);
        webSocketService.unregisterHandler(WebSocket.Messages.REQUEST_RECEIVED, requestReceivedHandler);
    }


    @Override
    protected Long getUpdatePeriod() {
        SharedPreferences prefs = UserSettings.get(getActivity());
        return prefs.getLong(UserSettings.REQUEST_UPDATE_PERIOD, UserSettings.DEFAULT_REQUEST_UPDATE_PERIOD);
    }

    @Override
    protected void requireWebUpdate() {
        webSocket.emit(WebSocket.Messages.GET_INCOMING_REQUESTS);
        setEmptyText(R.string.loading_list_requests);
    }

    @Override
    protected void setupWebSocketHandlers() {
        super.setupWebSocketHandlers();

        this.setupGetIncomingRequestsHandlers();
        this.setupAcceptRequestHandlers();
        this.setupDeclineRequestHandlers();
        this.setupRequestReceivedHandler();
    }

    private void setupGetIncomingRequestsHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                try {
                    JSONArray dataArray = (JSONArray) args[0];
                    JSONArray usersArray = dataArray.getJSONArray(0);
                    onGetIncomingRequestsSuccess(usersArray);
                } catch (JSONException e) { }
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                Log.i(TAG, "Incoming requests pull failed");
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.GET_INCOMING_REQUESTS, successHandler, failureHandler);
    }

    private void setupAcceptRequestHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                final int requestID = Integer.parseInt(args[0].toString());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        removeRequestFromList(requestID);
                        ToastHelper.showToastInUiThread(getActivity(), R.string.request_accepted);
                    }
                });
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                final String error = args[0].toString();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastHelper.showToastInUiThread(getActivity(), error);
                    }
                });
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.ACCEPT_REQUEST, successHandler, failureHandler);
    }

    private void setupDeclineRequestHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                final int requestID = Integer.parseInt(args[0].toString());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        removeRequestFromList(requestID);
                        ToastHelper.showToastInUiThread(getActivity(), R.string.request_declined);
                    }
                });
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                final String error = args[0].toString();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastHelper.showToastInUiThread(getActivity(), error);
                    }
                });
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.DECLINE_REQUEST, successHandler, failureHandler);
    }

    private void setupRequestReceivedHandler() {
        this.requestReceivedHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                requireWebUpdate();
            }
        };

        webSocketService.registerHandler(WebSocket.Messages.REQUEST_RECEIVED, requestReceivedHandler);
    }

    private void removeRequestFromList(int requestID) {
        ArrayList<RequestingUser> users = super.getUsers();
        ArrayAdapter adapter = super.getUserAdapter();
        int index = -1;

        for (int i = 0; i < users.size(); i++)
            if (users.get(i).getRequestID() == requestID) {
                index = i;
                break;
            }

        if (index >= 0) {
            users.remove(index);
            adapter.notifyDataSetChanged();
        }
    }


    private void onGetIncomingRequestsSuccess(JSONArray usersArray) {
        List<RequestingUser> newUsers = new ArrayList<RequestingUser>();

        for (int i = 0; i < usersArray.length(); i++) {
            try {
                JSONObject userObject = usersArray.getJSONObject(i);
                RequestingUser user = new RequestingUser();

                user.setID(userObject.getInt(UserAttributes.ID));
                user.setName(userObject.getString(UserAttributes.NAME));
                user.setRange(userObject.getInt(UserAttributes.RANGE));
                user.setDescription(userObject.getString(UserAttributes.DESCRIPTION));
                user.setGender(userObject.getString(UserAttributes.GENDER));
                user.setBirthday(userObject.getString(UserAttributes.BIRTHDAY));
                user.setPictureFile(getActivity());

                user.setRequestID(userObject.getInt(RequestingUserAttributes.REQUEST_ID));
                user.setRequestText(userObject.getString(RequestingUserAttributes.REQUEST_TEXT));
                user.setRequestIssueDate(userObject.getString(RequestingUserAttributes.REQUEST_ISSUE_DATE));

                newUsers.add(user);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ArrayList<RequestingUser> users = super.getUsers();
        users.clear();
        users.addAll(newUsers);

        setEmptyText(R.string.empty_list_requests);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getUserAdapter().notifyDataSetChanged();
            }
        });
    }


    @Override
    public void onListItemClick(ListView parent, View view, int position, long id) {
        super.onListItemClick(parent, view, position, id);

        final RequestingUser requestingUser = super.getUsers().get(position);
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

        alert.setTitle(getString(R.string.request_says, requestingUser.getName()));
        alert.setMessage(requestingUser.getRequestText());

        alert.setNeutralButton(R.string.cancel, null);

        alert.setNegativeButton(R.string.decline, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                webSocket.emit(WebSocket.Messages.DECLINE_REQUEST, requestingUser.getRequestID());
            }
        });

        alert.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                webSocket.emit(WebSocket.Messages.ACCEPT_REQUEST, requestingUser.getRequestID());
            }
        });

        alert.show();
    }
}
