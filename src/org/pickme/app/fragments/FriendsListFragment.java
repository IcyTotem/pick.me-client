package org.pickme.app.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pickme.R;
import org.pickme.app.ChatActivity;
import org.pickme.app.adapters.FriendAdapter;
import org.pickme.app.adapters.UserAdapter;
import org.pickme.entities.Friend;
import org.pickme.entities.Message;
import org.pickme.entities.attributes.MessageAttributes;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.entities.attributes.FriendAttributes;
import org.pickme.utils.ToastHelper;
import org.pickme.entities.attributes.UserAttributes;
import org.pickme.utils.UserSettings;

import java.util.ArrayList;
import java.util.List;

public class FriendsListFragment extends UserListFragment<Friend> {
    private static final String TAG = FriendsListFragment.class.getSimpleName();
    private static final String STATE_SORTING = "sorting";

    private Sorting sorting;
    private WebSocketEventHandler messageReceivedHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.GET_FRIENDS))
            this.setupGetFriendsHandlers();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.GET_UNREAD_MESSAGES_COUNT_BY_SENDER))
            this.setupGetUnreadMessagesCountsBySenderHandlers();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.UNFRIEND))
            this.setupUnfriendHandlers();

        if (!webSocketService.hasHandler(WebSocket.Messages.MESSAGE_RECEIVED, messageReceivedHandler))
            this.setupMessageReceivedHandler();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        webSocketService.unregisterCallbacks(WebSocket.Messages.GET_FRIENDS);
        webSocketService.unregisterCallbacks(WebSocket.Messages.GET_UNREAD_MESSAGES_COUNT_BY_SENDER);
        webSocketService.unregisterCallbacks(WebSocket.Messages.UNFRIEND);
        webSocketService.unregisterHandler(WebSocket.Messages.MESSAGE_RECEIVED, messageReceivedHandler);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(STATE_SORTING, sorting.ordinal());
    }

    @Override
    public Bundle onRestoreInstanceState(Bundle savedInstanceState) {
        Bundle bundle = super.onRestoreInstanceState(savedInstanceState);

        if (bundle != null) {
            this.sorting = Sorting.values()[bundle.getInt(STATE_SORTING)];
            this.notifyUiDataSetChanged();
        } else
            this.sorting = Sorting.NONE;

        if (this.getUsers().size() == 0)
            this.setEmptyText(R.string.empty_list_friends);

        return bundle;
    }

    @Override
    protected void setupWebSocketHandlers() {
        super.setupWebSocketHandlers();

        this.setupGetFriendsHandlers();
        this.setupGetUnreadMessagesCountsBySenderHandlers();
        this.setupUnfriendHandlers();
        this.setupMessageReceivedHandler();
    }

    private void setupGetFriendsHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                try {
                    JSONArray dataArray = (JSONArray)args[0];
                    JSONArray usersArray = dataArray.getJSONArray(0);
                    onGetFriendsSuccess(usersArray);
                } catch (JSONException e) { }
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                Log.i(TAG, "Friends pull failed");
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.GET_FRIENDS, successHandler, failureHandler);
    }

    private void setupGetUnreadMessagesCountsBySenderHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                try {
                    JSONArray dataArray = (JSONArray)args[0];
                    JSONArray countsArray = dataArray.getJSONArray(0);
                    onGetUnreadMessagesCountsBySender(countsArray);
                } catch (JSONException e) { }
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                Log.i(TAG, "Unread messages counts pull failed");
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.GET_UNREAD_MESSAGES_COUNT_BY_SENDER, successHandler, failureHandler);
    }

    private void setupUnfriendHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                Integer friendID = Integer.parseInt(args[0].toString());
                onUnfriendSuccess(friendID);
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String error = args[0].toString();
                ToastHelper.showToastInUiThread(getActivity(), error);
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.UNFRIEND, successHandler, failureHandler);
    }

    private void setupMessageReceivedHandler() {
        this.messageReceivedHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                JSONObject messageObject = (JSONObject)args[0];
                onMessageReceived(messageObject);
            }
        };

        webSocketService.registerHandler(WebSocket.Messages.MESSAGE_RECEIVED, messageReceivedHandler);
    }


    private void onGetFriendsSuccess(JSONArray usersArray) {
        List<Friend> newUsers = new ArrayList<Friend>();

        for (int i = 0; i < usersArray.length(); i++) {
            try {
                JSONObject userObject = usersArray.getJSONObject(i);
                Friend user = new Friend();

                user.setID(userObject.getInt(UserAttributes.ID));
                user.setName(userObject.getString(UserAttributes.NAME));
                user.setRange(userObject.getInt(UserAttributes.RANGE));
                user.setDescription(userObject.getString(UserAttributes.DESCRIPTION));
                user.setGender(userObject.getString(UserAttributes.GENDER));
                user.setBirthday(userObject.getString(UserAttributes.BIRTHDAY));
                user.setLastUpdate(userObject.getString(UserAttributes.LAST_UPDATE));
                user.setPictureFile(getActivity());

                newUsers.add(user);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ArrayList<Friend> users = super.getUsers();
        users.clear();
        users.addAll(newUsers);

        this.setEmptyText(R.string.empty_list_friends);
        this.notifyUiDataSetChanged();

        webSocket.emit(WebSocket.Messages.GET_UNREAD_MESSAGES_COUNT_BY_SENDER);
    }

    private void onGetUnreadMessagesCountsBySender(JSONArray countsArray) {
        for (int i = 0; i < countsArray.length(); i++) {
            try {
                JSONObject row = countsArray.getJSONObject(i);
                Integer senderID = row.getInt(FriendAttributes.UNREAD_MSG_SENDER_USER_ID);
                Integer count = row.getInt(FriendAttributes.UNREAD_MSG_COUNT);
                Friend sender = this.findUserByID(senderID);
                if (sender != null)
                    sender.setUnreadMessagesCount(count);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        this.notifyUiDataSetChanged();
    }

    private void onUnfriendSuccess(int friendID) {
        int index = this.findUserIndexByID(friendID);

        if (index < 0)
            return;

        final List<Friend> friends = this.getUsers();
        final Friend user = friends.get(index);

        friends.remove(index);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getUserAdapter().notifyDataSetChanged();
                ToastHelper.showToast(getActivity(), R.string.unfriend_success_message, user.getName());
            }
        });
    }

    private void onMessageReceived(JSONObject messageObject) {
        Integer senderID = null;

        try {
            senderID = messageObject.getInt(MessageAttributes.SENDER_USER_ID);
            // Other attributes are not important here
        } catch (JSONException e) {
            return;
        }

        // If chat window is open and you are chatting with the sender of this message, do nothing
        if (ChatActivity.isVisible()) {
            Friend target = ChatActivity.getCurrentTarget();
            if ((target != null) && (target.getID() == senderID))
                return;
        }

        Friend friend = super.findUserByID(senderID);

        if (friend != null) {
            friend.setUnreadMessagesCount(friend.getUnreadMessagesCount() + 1);
            this.notifyUiDataSetChanged();
        }
    }


    private void notifyUiDataSetChanged() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (sorting.equals(Sorting.BY_RANGE))
                    sortByRange();
                else if (sorting.equals(Sorting.BY_LAST_UPDATE))
                    sortByLastUpdate();
                else if (sorting.equals(Sorting.BY_UNREAD_MESSAGES_COUNT))
                    sortByUnreadMessagesCount();
                else
                    getUserAdapter().notifyDataSetChanged();
            }
        });
    }


    @Override
    protected UserAdapter createUserAdapter() {
        return new FriendAdapter(getActivity(), getUsers());
    }

    @Override
    protected Long getUpdatePeriod() {
        SharedPreferences prefs = UserSettings.get(getActivity());
        return prefs.getLong(UserSettings.FRIEND_UPDATE_PERIOD, UserSettings.DEFAULT_FRIEND_UPDATE_PERIOD);
    }

    @Override
    protected void requireWebUpdate() {
        webSocket.emit(WebSocket.Messages.GET_FRIENDS);
        setEmptyText(R.string.loading_list_friends);
    }


    public void sortByLastUpdate() {
        UserAdapter adapter = super.getUserAdapter();

        this.sorting = Sorting.BY_LAST_UPDATE;

        adapter.sort(Friend.LAST_UPDATE_COMPARATOR);
        adapter.notifyDataSetChanged();
    }

    public void sortByRange() {
        UserAdapter adapter = super.getUserAdapter();

        this.sorting = Sorting.BY_RANGE;

        adapter.sort(Friend.RANGE_COMPARATOR);
        adapter.notifyDataSetChanged();
    }

    public void sortByUnreadMessagesCount() {
        UserAdapter adapter = super.getUserAdapter();

        this.sorting = Sorting.BY_UNREAD_MESSAGES_COUNT;

        adapter.sort(Friend.UNREAD_MESSAGES_COUNT_COMPARATOR);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onListItemClick(ListView parent, View view, int position, long id) {
        super.onListItemClick(parent, view, position, id);

        Friend friend = super.getUsers().get(position);
        Intent intent = new Intent(getActivity(), ChatActivity.class);

        intent.putExtra(ChatActivity.INTENT_TARGET, friend);

        friend.setUnreadMessagesCount(0); // By opening the chat window, we read last messages
        this.notifyUiDataSetChanged();

        startActivity(intent);
    }

    @Override
    protected void onItemLongClick(final AdapterView<?> adapterView, final View view, final int position, final long id) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final Friend target = this.getUsers().get(position);

        builder.setTitle(getString(R.string.friend_click_dialog_title));

        if (target.getGender().startsWith("f"))
            builder.setMessage(getString(R.string.friend_click_dialog_body_f, target.getName()));
        else
            builder.setMessage(getString(R.string.friend_click_dialog_body_m, target.getName()));

        builder.setNeutralButton(R.string.cancel, null);

        builder.setNegativeButton(R.string.blacklist, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FriendsListFragment.super.onItemLongClick(adapterView, view, position, id);
            }
        });

        builder.setPositiveButton(R.string.unfriend, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                WebSocket webSocket = webSocketService.getWebSocket();
                webSocket.emit(WebSocket.Messages.UNFRIEND, target.getID());
            }
        });

        Dialog dialog = builder.create();
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.show();
    }

    private enum Sorting {
        NONE,
        BY_RANGE,
        BY_LAST_UPDATE,
        BY_UNREAD_MESSAGES_COUNT
    }
}
