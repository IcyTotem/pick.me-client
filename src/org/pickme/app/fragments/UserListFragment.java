package org.pickme.app.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.InputType;
import android.view.*;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import org.pickme.R;
import org.pickme.app.adapters.UserAdapter;
import org.pickme.entities.User;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.service.WebSocketService;
import org.pickme.utils.ToastHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class UserListFragment<T extends User> extends ListFragment {
    private static final String STATE_SAVED = "saved";
    private static final String STATE_USERS = "users";
    private static final String STATE_LAST_UPDATE_TIME = "last-update-time";

    private ArrayList<T> users;
    private UserAdapter adapter;

    private ScheduledExecutorService updateScheduler;
    private Object lastUpdateLock = new Object();
    private long lastUpdateTime = 0;
    private long initialScheduleDelay = 0;

    private static Map<Class, Bundle> staticSavedStates = new HashMap<Class, Bundle>();
    private boolean instanceSaved;
    private boolean viewCreated;

    protected WebSocketService webSocketService;
    protected WebSocket webSocket;


    public UserAdapter getUserAdapter() {
        return adapter;
    }

    public ArrayList<T> getUsers() {
        return users;
    }

    protected UserAdapter createUserAdapter() {
        return new UserAdapter(getActivity(), users);
    }


    public UserListFragment() {
        this.instanceSaved = false;
        this.viewCreated = false;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.users = new ArrayList<T>();
        this.adapter = this.createUserAdapter();
        this.setListAdapter(adapter);

        this.webSocketService = WebSocketService.getInstance();
        this.webSocket = webSocketService.getWebSocket();
        this.setupWebSocketHandlers();

        this.onRestoreInstanceState(savedInstanceState);
        this.scheduleUpdates();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                UserListFragment.this.onItemLongClick(adapterView, view, position, id);
                return true;
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        viewCreated = false;

        updateScheduler.shutdownNow();
        webSocketService.unregisterCallbacks(WebSocket.Messages.BLACKLIST);

        if (!instanceSaved) {
            Bundle staticState = this.getStaticState();
            onSaveInstanceState(staticState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putBoolean(STATE_SAVED, true);
        savedInstanceState.putParcelableArrayList(STATE_USERS, users);

        synchronized (lastUpdateLock) {
            savedInstanceState.putLong(STATE_LAST_UPDATE_TIME, lastUpdateTime);
        }

        this.instanceSaved = true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        viewCreated = true;
        return super.onCreateView(inflater, container, bundle);
    }

    public Bundle onRestoreInstanceState(Bundle savedInstanceState) {
        this.instanceSaved = false;

        if (savedInstanceState == null) {
            Bundle staticState = this.getStaticState();
            if (staticState == null)
                return null;
            else
                savedInstanceState = staticState;
        }

        if (!savedInstanceState.getBoolean(STATE_SAVED, false))
            return null;

        long updatePeriod = this.getUpdatePeriod();
        long elapsedTimeSinceLastUpdate = System.currentTimeMillis() - lastUpdateTime;

        this.lastUpdateTime = savedInstanceState.getLong(STATE_LAST_UPDATE_TIME);

        // If more time has passed since the last update than the update period length, then
        // onCreate will require an update instantly like always
        if (elapsedTimeSinceLastUpdate > updatePeriod)
            return null;

        // Otherwise, fetch stored user list and display it, without issuing any network request
        ArrayList<T> savedUsers = savedInstanceState.getParcelableArrayList(STATE_USERS);
        users.addAll(savedUsers);
        adapter.notifyDataSetChanged();

        // The next update will be scheduled starting when the update period is correctly elapsed
        this.initialScheduleDelay = updatePeriod - elapsedTimeSinceLastUpdate;

        return savedInstanceState;
    }

    private void scheduleUpdates() {
        long updatePeriod = this.getUpdatePeriod();

        this.updateScheduler = Executors.newScheduledThreadPool(1);
        this.updateScheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                synchronized (lastUpdateLock) {
                    lastUpdateTime = System.currentTimeMillis();
                    requireWebUpdate();
                }
            }
        }, initialScheduleDelay, updatePeriod, TimeUnit.MILLISECONDS);
    }

    private Bundle getStaticState() {
        Class klass = this.getClass();

        if (staticSavedStates.containsKey(klass))
            return staticSavedStates.get(klass);

        Bundle bundle = new Bundle();
        staticSavedStates.put(klass, bundle);

        return bundle;
    }


    protected void setupWebSocketHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                int userID = Integer.parseInt(args[0].toString());
                onBlacklistSuccess(userID);
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String error = args[0].toString();
                ToastHelper.showToastInUiThread(getActivity(), error);
            }
        };

        this.webSocketService.registerCallbacks(WebSocket.Messages.BLACKLIST, successHandler, failureHandler);
    }


    protected void onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
        showBlacklistingDialog(position);
    }

    private void showBlacklistingDialog(int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final T target = users.get(position);

        builder.setTitle(getString(R.string.blacklisting_dialog_title));
        builder.setMessage(getString(R.string.blacklisting_dialog_body, target.getName()));

        final EditText input = new EditText(getActivity());
        input.setSingleLine();
        input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        builder.setView(input);

        builder.setNegativeButton(R.string.cancel, null);
        builder.setPositiveButton(R.string.blacklist, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                WebSocket webSocket = webSocketService.getWebSocket();
                webSocket.emit(WebSocket.Messages.BLACKLIST, target.getID(), input.getText().toString());
            }
        });

        Dialog dialog = builder.create();
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.show();
    }

    private void onBlacklistSuccess(int userID) {
        int index = this.findUserIndexByID(userID);

        if (index < 0)
            return;

        final T user = users.get(index);
        users.remove(index);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                ToastHelper.showToastInUiThread(getActivity(), R.string.blacklisting_success_message, user.getName());
            }
        });
    }


    protected T findUserByID(int userID) {
        for (T user : users)
            if (user.getID() == userID)
                return user;
        return null;
    }

    protected int findUserIndexByID(int userID) {
        for (int i = 0; i < users.size(); i++)
            if (users.get(i).getID() == userID)
                return i;
        return -1;
    }

    protected void setEmptyText(final int stringID) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isAdded() && viewCreated)
                    setEmptyText(getString(stringID));
            }
        });
    }


    @Override
    public void onListItemClick(ListView parent, View view, int position, long id) {
        super.onListItemClick(parent, view, position, id);
    }

    protected abstract Long getUpdatePeriod();
    protected abstract void requireWebUpdate();

    public void forceWebUpdate() {
        this.requireWebUpdate();
    }
}
