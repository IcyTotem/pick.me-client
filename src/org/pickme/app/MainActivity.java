package org.pickme.app;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import org.pickme.R;
import org.pickme.app.base.BaseFragmentActivity;
import org.pickme.app.dialogs.EditProfileDataActivity;
import org.pickme.app.dialogs.EditSettingsActivity;
import org.pickme.app.dialogs.GatherUserPreferenceActivity;
import org.pickme.app.fragments.FriendsListFragment;
import org.pickme.app.fragments.NearbyUsersListFragment;
import org.pickme.app.fragments.RequestsListFragment;
import org.pickme.service.*;

public class MainActivity extends BaseFragmentActivity implements ActionBar.TabListener {
    public static final String ACTION_SHOW_NEARBY_USERS = "SHOW_NEARBY_USERS";
    public static final String ACTION_SHOW_REQUESTS = "SHOW_REQUESTS";
    public static final String ACTION_SHOW_FRIENDS = "SHOW_FRIENDS";

    private WebSocketService webSocketService;

    private ActionBar.Tab tabNearbyUsers, tabRequests, tabFriends;
    private NearbyUsersListFragment nearbyUsersFragment;
    private RequestsListFragment requestFragment;
    private FriendsListFragment friendsFragment;

    private ActionBar.Tab selectedTab;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        this.webSocketService = WebSocketService.getInstance();

        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        tabNearbyUsers = actionBar.newTab().setText(getString(R.string.tab_nearby));
        tabRequests = actionBar.newTab().setText(getString(R.string.tab_requests));
        tabFriends = actionBar.newTab().setText(getString(R.string.tab_friends));

        nearbyUsersFragment = new NearbyUsersListFragment();
        requestFragment = new RequestsListFragment();
        friendsFragment = new FriendsListFragment();

        tabNearbyUsers.setTabListener(this);
        tabRequests.setTabListener(this);
        tabFriends.setTabListener(this);

        actionBar.addTab(tabNearbyUsers);
        actionBar.addTab(tabRequests);
        actionBar.addTab(tabFriends);
    }

    @Override
    public void onStart() {
        super.onStart();

        Intent intent = getIntent();

        if (!webSocketService.hasHandler(WebSocket.Messages.NOT_LOGGED))
            this.setupWebSocketHandlers();

        if (intent == null)
            return;

        String action = intent.getAction();
        ActionBar actionBar = getActionBar();

        if (ACTION_SHOW_NEARBY_USERS.equals(action))
            actionBar.selectTab(tabNearbyUsers);
        else if (ACTION_SHOW_REQUESTS.equals(action))
            actionBar.selectTab(tabRequests);
        else if (ACTION_SHOW_FRIENDS.equals(action))
            actionBar.selectTab(tabFriends);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        webSocketService.unregisterHandlers(WebSocket.Messages.NOT_LOGGED);
    }

    private void setupWebSocketHandlers() {
        WebSocketEventHandler handler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onNotLogged();
                    }
                });
            }
        };

        webSocketService.registerHandler(WebSocket.Messages.NOT_LOGGED, handler);
    }


    private void onNotLogged() {
        if (ChatActivity.isVisible())
            ChatActivity.getInstance().finish();

        // Quiet login doesn't erase session data, therefore an automatic login can be performed by StartActivity
        if (LoginService.isAvailable())
            LoginService.getInstance().quietLogout();
        else
            LoginService.forceQuietLogout();

        this.finish();

        Application.startStartActivityFrom(this);
    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        this.selectedTab = tab;

        if (tab.equals(tabNearbyUsers))
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, nearbyUsersFragment).commit();
        else if (tab.equals(tabRequests))
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, requestFragment).commit();
        else if (tab.equals(tabFriends))
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, friendsFragment).commit();

        this.invalidateOptionsMenu();
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        this.selectedTab = null;

        if (tab.equals(tabNearbyUsers))
            getSupportFragmentManager().beginTransaction().remove(nearbyUsersFragment).commit();
        else if (tab.equals(tabRequests))
            getSupportFragmentManager().beginTransaction().remove(requestFragment).commit();
        else if (tab.equals(tabFriends))
            getSupportFragmentManager().beginTransaction().remove(friendsFragment).commit();
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        boolean isNearbyUsersTab = tabNearbyUsers.equals(selectedTab);
        boolean isRequestsTab = tabRequests.equals(selectedTab);
        boolean isFriendsTab = tabFriends.equals(selectedTab);

        menu.findItem(R.id.menuSortByRange).setVisible(isNearbyUsersTab || isFriendsTab);
        menu.findItem(R.id.menuSortByLastUpdate).setVisible(isNearbyUsersTab || isFriendsTab);
        menu.findItem(R.id.menuSortByUnreadMessagesCount).setVisible(isFriendsTab);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menuEdit:
                startActivity(new Intent(getApplicationContext(), EditProfileDataActivity.class));
                return true;

            case R.id.menuSearchCriteria:
                startActivity(new Intent(getApplicationContext(), GatherUserPreferenceActivity.class));
                return true;

            case R.id.menuSettings:
                startActivity(new Intent(getApplicationContext(), EditSettingsActivity.class));
                return true;

            case R.id.menuLogout:
                this.performLogout();
                return true;

            case R.id.menuSortByRange:
                if (tabNearbyUsers.equals(selectedTab))
                    nearbyUsersFragment.sortByRange();
                else if (tabFriends.equals(selectedTab))
                    friendsFragment.sortByRange();
                return true;

            case R.id.menuSortByLastUpdate:
                if (tabNearbyUsers.equals(selectedTab))
                    nearbyUsersFragment.sortByLastUpdate();
                else if (tabFriends.equals(selectedTab))
                    friendsFragment.sortByLastUpdate();
                return true;

            case R.id.menuSortByUnreadMessagesCount:
                friendsFragment.sortByUnreadMessagesCount();
                return true;

            case R.id.menuRefresh:
                if (tabNearbyUsers.equals(selectedTab))
                    nearbyUsersFragment.forceWebUpdate();
                else if (tabRequests.equals(selectedTab))
                    requestFragment.forceWebUpdate();
                else if (tabFriends.equals(selectedTab))
                    friendsFragment.forceWebUpdate();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        // Do nothing: the only way to exit this activity is by loggin out explicitly
    }


    private void performLogout() {
        if (LoginService.isAvailable())
            LoginService.getInstance().logout();
        else
            LoginService.forceLogout(this);

        this.finish();

        Application.startStartMenuActivityFrom(this);
    }
}