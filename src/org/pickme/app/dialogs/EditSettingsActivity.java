package org.pickme.app.dialogs;

import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.*;
import org.pickme.R;
import org.pickme.app.base.BaseActivity;
import org.pickme.service.GeolocationService;
import org.pickme.utils.Globals;
import org.pickme.utils.UserSettings;

import java.util.HashMap;
import java.util.Map;

public class EditSettingsActivity extends BaseActivity {
    private Button btnConfirm;
    private Spinner spinProvider;
    private SeekBar sbLocationRefreshPeriod, sbLocationRefreshDistance, sbRequestsPullPeriod, sbFriendsPullPeriod;
    private TextView txtLocationRefreshPeriod, txtLocationRefreshDistance, txtRequestsPullPeriod, txtFriendsPullPeriod;

    private Map<SeekBar, TextView> displayMap;
    private Map<SeekBar, Long> valueMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.edit_settings);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        this.btnConfirm = (Button)findViewById(R.id.btnConfirm);
        this.spinProvider = (Spinner)findViewById(R.id.spinProvider);
        
        this.sbLocationRefreshPeriod = (SeekBar)findViewById(R.id.sbLocationRefreshPeriod);
        this.sbLocationRefreshDistance = (SeekBar)findViewById(R.id.sbLocationRefreshDistance);
        this.sbRequestsPullPeriod = (SeekBar)findViewById(R.id.sbRequestsPullPeriod);
        this.sbFriendsPullPeriod = (SeekBar)findViewById(R.id.sbFriendsPullPeriod);

        this.txtLocationRefreshPeriod = (TextView)findViewById(R.id.txtLocationRefreshPeriod);
        this.txtLocationRefreshDistance = (TextView)findViewById(R.id.txtLocationRefreshDistance);
        this.txtRequestsPullPeriod = (TextView)findViewById(R.id.txtRequestsPullPeriod);
        this.txtFriendsPullPeriod = (TextView)findViewById(R.id.txtFriendsPullPeriod);

        this.setupSpinner();
        this.setupSeekBars();
        this.loadSettings();

        this.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnConfirmOnClick(view);
            }
        });
    }

    private void setupSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.location_providers, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinProvider.setAdapter(adapter);
    }

    private void setupSeekBars() {
        this.displayMap = new HashMap<SeekBar, TextView>();
        this.displayMap.put(sbLocationRefreshPeriod, txtLocationRefreshPeriod);
        this.displayMap.put(sbRequestsPullPeriod, txtRequestsPullPeriod);
        this.displayMap.put(sbFriendsPullPeriod, txtFriendsPullPeriod);

        this.valueMap = new HashMap<SeekBar, Long>();

        for (SeekBar seekBar : displayMap.keySet()) {
            final TextView textView = displayMap.get(seekBar);

            seekBar.setMax(Globals.MAX_REFRESH_PERIOD - Globals.MIN_REFRESH_PERIOD);
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    Integer value = Globals.MIN_REFRESH_PERIOD + progress;
                    valueMap.put(seekBar, value * 60 * 1000L);
                    textView.setText(value + "m");
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) { }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) { }
            });
        }

        sbLocationRefreshDistance.setMax(Globals.MAX_RANGE - Globals.MIN_RANGE);
        sbLocationRefreshDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Integer value = Globals.MIN_RANGE + progress;
                valueMap.put(seekBar, (long)value);

                if (value < 1000)
                    txtLocationRefreshDistance.setText(value + "m");
                else
                    txtLocationRefreshDistance.setText(String.format("%.1f", (float) value / 1000.0f) + "km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
    }

    private void loadSettings() {
        SharedPreferences prefs = UserSettings.get(this);
        String provider = prefs.getString(UserSettings.LOCATION_PROVIDER, UserSettings.DEFAULT_LOCATION_PROVIDER);
        Long locationRefreshPeriod = prefs.getLong(UserSettings.LOCATION_UPDATE_PERIOD, UserSettings.DEFAULT_LOCATION_UPDATE_PERIOD);
        Long locationRefreshDistance = prefs.getLong(UserSettings.LOCATION_UPDATE_DISTANCE, UserSettings.DEFAULT_LOCATION_UPDATE_DISTANCE);
        Long requestsPullPeriod = prefs.getLong(UserSettings.REQUEST_UPDATE_PERIOD, UserSettings.DEFAULT_REQUEST_UPDATE_PERIOD);
        Long friendsPullPeriod = prefs.getLong(UserSettings.FRIEND_UPDATE_PERIOD, UserSettings.DEFAULT_FRIEND_UPDATE_PERIOD);

        if (provider.toLowerCase().equals("gps"))
            spinProvider.setSelection(1);
        else
            spinProvider.setSelection(0);

        sbLocationRefreshDistance.setProgress((int)(locationRefreshDistance - Globals.MIN_RANGE));
        sbLocationRefreshPeriod.setProgress((int)(locationRefreshPeriod / 60000) - Globals.MIN_REFRESH_PERIOD);
        sbRequestsPullPeriod.setProgress((int)(requestsPullPeriod / 60000) - Globals.MIN_REFRESH_PERIOD);
        sbFriendsPullPeriod.setProgress((int)(friendsPullPeriod / 60000) - Globals.MIN_REFRESH_PERIOD);

        this.valueMap.put(sbLocationRefreshDistance, locationRefreshDistance);
        this.valueMap.put(sbLocationRefreshPeriod, locationRefreshPeriod);
        this.valueMap.put(sbRequestsPullPeriod, requestsPullPeriod);
        this.valueMap.put(sbFriendsPullPeriod, friendsPullPeriod);
    }


    private void btnConfirmOnClick(View view) {
        String provider = spinProvider.getSelectedItem().toString().toLowerCase().equals("gps") ? LocationManager.GPS_PROVIDER : LocationManager.NETWORK_PROVIDER;
        Long locationRefreshPeriod = valueMap.get(sbLocationRefreshPeriod);
        Long locationRefreshDistance = valueMap.get(sbLocationRefreshDistance);
        Long requestsPullPeriod = valueMap.get(sbRequestsPullPeriod);
        Long friendsPullPeriod = valueMap.get(sbFriendsPullPeriod);

        SharedPreferences.Editor editor = UserSettings.edit(this);
        editor.putString(UserSettings.LOCATION_PROVIDER, provider);
        editor.putLong(UserSettings.LOCATION_UPDATE_DISTANCE, locationRefreshDistance);
        editor.putLong(UserSettings.LOCATION_UPDATE_PERIOD, locationRefreshPeriod);
        editor.putLong(UserSettings.REQUEST_UPDATE_PERIOD, requestsPullPeriod);
        editor.putLong(UserSettings.FRIEND_UPDATE_PERIOD, friendsPullPeriod);
        editor.apply();

        GeolocationService.getInstance().reloadUpdatePeriods();

        this.finish();
    }
}