package org.pickme.app.dialogs;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.*;
import org.pickme.R;
import org.pickme.app.base.BaseActivity;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.utils.Globals;
import org.pickme.utils.ToastHelper;
import org.pickme.entities.attributes.UserAttributes;

public class GatherUserPreferenceActivity extends BaseActivity {
    private EditText txtAgeMin, txtAgeMax;
    private Spinner spinGender;
    private SeekBar sbRange;
    private TextView txtRange;
    private Button btnConfirm;

    private int range = Globals.MIN_RANGE;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.gather_user_preference);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        this.txtAgeMin = (EditText)findViewById(R.id.txtAgeMin);
        this.txtAgeMax = (EditText)findViewById(R.id.txtAgeMax);
        this.spinGender = (Spinner)findViewById(R.id.spinGender);
        this.sbRange = (SeekBar)findViewById(R.id.sbRange);
        this.txtRange = (TextView)findViewById(R.id.txtRange);
        this.btnConfirm = (Button)findViewById(R.id.btnConfirm);

        this.hookWebSocketService();

        this.setupSpinner();
        this.setupSeekBar();
        this.setupAgeBoxes();

        this.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnConfirmOnClick(view);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        service.unregisterCallbacks(WebSocket.Messages.UPDATE_PREFERENCE);
    }

    @Override
    public void onStart() {
        super.onStart();

        SharedPreferences prefs = UserAttributes.get(this);

        if (prefs.getString(UserAttributes.LOOKING_FOR_GENDER, "m").startsWith("m"))
            spinGender.setSelection(0);
        else
            spinGender.setSelection(1);

        txtAgeMin.setText(prefs.getString(UserAttributes.LOOKING_FOR_AGE_MIN, "16"));
        txtAgeMax.setText(prefs.getString(UserAttributes.LOOKING_FOR_AGE_MAX, "99"));
        txtRange.setText(prefs.getString(UserAttributes.LOOKING_FOR_RANGE, "50") + "m");

        Integer range = Integer.parseInt(prefs.getString(UserAttributes.LOOKING_FOR_RANGE, "50"));
        sbRange.setProgress(range - Globals.MIN_RANGE);
    }

    private void setupSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.genders, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinGender.setAdapter(adapter);
    }

    private void setupSeekBar() {
        sbRange.setMax(Globals.MAX_RANGE - Globals.MIN_RANGE);

        this.sbRange.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                range = Globals.MIN_RANGE + progress;
                txtRange.setText(range + "m");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setupAgeBoxes() {
        View.OnFocusChangeListener focusListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) validateAgeBoxes(view);
            }
        };

        this.txtAgeMin.setOnFocusChangeListener(focusListener);
        this.txtAgeMax.setOnFocusChangeListener(focusListener);
    }


    @Override
    protected void setupWebSocketHandlers() {
        final Context context = this;

        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onUpdatePreferenceSuccess();
                    }
                });
            }
        };
        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                final String error = args[0].toString();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onUpdatePreferenceFailure(error);
                    }
                });
            }
        };

        this.service.registerCallbacks(WebSocket.Messages.UPDATE_PREFERENCE, successHandler, failureHandler);
    }

    private void onUpdatePreferenceSuccess() {
        SharedPreferences.Editor editor = UserAttributes.edit(this);
        editor.putString(UserAttributes.LOOKING_FOR_GENDER, this.getSelectedGender());
        editor.putString(UserAttributes.LOOKING_FOR_AGE_MIN, String.valueOf(this.getChosenAgeMin()));
        editor.putString(UserAttributes.LOOKING_FOR_AGE_MAX, String.valueOf(this.getChosenAgeMax()));
        editor.putString(UserAttributes.LOOKING_FOR_RANGE, String.valueOf(this.getChosenRange()));
        editor.putBoolean(UserAttributes.LOOKING_FOR_COMPLETED, true);
        editor.apply();

        setControlsEnabled(true);

        this.setResult(RESULT_OK);
        this.finish();
    }

    private void onUpdatePreferenceFailure(String error) {
        setControlsEnabled(true);
        ToastHelper.showToastInUiThread(this, error);
    }


    private void validateAgeBoxes(View view) {
        String ageMinText = txtAgeMin.getText().toString();
        String ageMaxText = txtAgeMax.getText().toString();

        int ageMin = ageMinText.equals("") ? Globals.MIN_AGE : Integer.parseInt(ageMinText);
        int ageMax = ageMaxText.equals("") ? 99 : Integer.parseInt(ageMaxText);

        if (ageMin > ageMax) {
            int temp = ageMin;
            ageMin = ageMax;
            ageMax = temp;
        }

        if (ageMin < Globals.MIN_AGE)
            ageMin = Globals.MIN_AGE;

        if (ageMax > 99)
            ageMax = 99;

        txtAgeMin.setText(String.valueOf(ageMin));
        txtAgeMax.setText(String.valueOf(ageMax));
    }

    private void btnConfirmOnClick(View view) {
        validateAgeBoxes(view);

        Preference pref = new Preference();
        pref.setLookingForGender(this.getSelectedGender());
        pref.setLookingForAgeMin(this.getChosenAgeMin());
        pref.setLookingForAgeMax(this.getChosenAgeMax());
        pref.setLookingForRange(this.getChosenRange());

        setControlsEnabled(false);

        webSocket.emit(WebSocket.Messages.UPDATE_PREFERENCE, pref);
    }


    private int getChosenAgeMin() {
        String ageMinText = txtAgeMin.getText().toString();
        return Integer.parseInt(ageMinText);
    }

    private int getChosenAgeMax() {
        String ageMaxText = txtAgeMax.getText().toString();
        return Integer.parseInt(ageMaxText);
    }

    private String getSelectedGender() {
        return spinGender.getSelectedItem().toString().toLowerCase().substring(0, 1);
    }

    private int getChosenRange() {
        return range;
    }


    private static class Preference {
        private String lookingForGender;
        private int lookingForAgeMin, lookingForAgeMax, lookingForRange;

        public String getLookingForGender() {
            return lookingForGender;
        }

        public void setLookingForGender(String lookingForGender) {
            this.lookingForGender = lookingForGender;
        }

        public int getLookingForAgeMin() {
            return lookingForAgeMin;
        }

        public void setLookingForAgeMin(int lookingForAgeMin) {
            this.lookingForAgeMin = lookingForAgeMin;
        }

        public int getLookingForAgeMax() {
            return lookingForAgeMax;
        }

        public void setLookingForAgeMax(int lookingForAgeMax) {
            this.lookingForAgeMax = lookingForAgeMax;
        }

        public int getLookingForRange() {
            return lookingForRange;
        }

        public void setLookingForRange(int lookingForRange) {
            this.lookingForRange = lookingForRange;
        }
    }
}