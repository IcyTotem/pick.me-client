package org.pickme.app.dialogs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import org.pickme.R;
import org.pickme.app.base.BaseActivity;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.utils.ToastHelper;
import org.pickme.entities.attributes.UserAttributes;

public class EditProfileDataActivity extends BaseActivity {
    private Button btnConfirm, btnSelectPicture;
    private EditText txtDescription;
    private Spinner spinGender;
    private EditText txtCountryCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.edit_profile_data);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        this.hookWebSocketService();

        this.btnConfirm = (Button)findViewById(R.id.btnConfirm);
        this.txtDescription = (EditText)findViewById(R.id.txtDescription);
        this.spinGender = (Spinner)findViewById(R.id.spinGender);
        this.txtCountryCode = (EditText)findViewById(R.id.txtCountryCode);
        this.btnSelectPicture = (Button)findViewById(R.id.btnSelectPicture);

        this.txtCountryCode.setText(this.getUserCountry());

        this.setupSpinner();

        this.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnConfirmOnClick(view);
            }
        });
        this.btnSelectPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), SelectPictureActivity.class));
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        SharedPreferences prefs = UserAttributes.get(this);
        String desc = prefs.getString(UserAttributes.DESCRIPTION, "");

        if (!desc.equals("null"))
            txtDescription.setText(desc);

        txtCountryCode.setText(prefs.getString(UserAttributes.COUNTRY, this.getUserCountry()));

        if (prefs.getString(UserAttributes.GENDER, "m").startsWith("m"))
            spinGender.setSelection(0);
        else
            spinGender.setSelection(1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        service.unregisterCallbacks(WebSocket.Messages.UPDATE_PROFILE);
    }


    @Override
    protected void setupWebSocketHandlers() {
        final Context context = this;

        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onProfileUpdateSuccess();
                    }
                });
            }
        };
        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                final String error = args[0].toString();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onUpdateProfileFailure(error);
                    }
                });
            }
        };

        this.service.registerCallbacks(WebSocket.Messages.UPDATE_PROFILE, successHandler, failureHandler);
    }

    private void setupSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.genders, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinGender.setAdapter(adapter);
    }


    public void btnConfirmOnClick(View view) {
        String countryCode = this.getChosenCountryCode();

        if (countryCode.equals("") || (countryCode.length() != 2)) {
            this.showToast(R.string.error_invalid_country_code);
            return;
        }

        this.setControlsEnabled(false);

        Profile p = new Profile();
        p.setDescription(this.getChosenDescription());
        p.setCountryCode(this.getChosenCountryCode());
        p.setGender(this.getSelectedGender());

        webSocket.emit(WebSocket.Messages.UPDATE_PROFILE, p);
    }


    private void onUpdateProfileFailure(String error) {
        this.setControlsEnabled(true);
        ToastHelper.showToastInUiThread(this, error);
    }

    private void onProfileUpdateSuccess() {
        SharedPreferences.Editor editor = UserAttributes.edit(this);

        editor.putString(UserAttributes.DESCRIPTION, this.getChosenDescription());
        editor.putString(UserAttributes.COUNTRY, this.getChosenCountryCode());
        editor.putString(UserAttributes.GENDER, this.getSelectedGender());
        editor.apply();

        this.setControlsEnabled(true);

        this.setResult(RESULT_OK);
        this.finish();
    }


    public String getChosenDescription() {
        return txtDescription.getText().toString();
    }

    public String getSelectedGender() {
        return spinGender.getSelectedItem().toString().toLowerCase().substring(0, 1);
    }

    public String getChosenCountryCode() {
        return txtCountryCode.getText().toString().toUpperCase();
    }


    private static class Profile {
        private String description, countryCode, gender;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }
    }
}