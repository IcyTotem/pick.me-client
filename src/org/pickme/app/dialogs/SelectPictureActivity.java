package org.pickme.app.dialogs;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import org.pickme.R;
import org.pickme.app.base.BaseActivity;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.utils.Globals;
import org.pickme.utils.ToastHelper;
import org.pickme.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class SelectPictureActivity extends BaseActivity  {
    private static final int SELECT_PHOTO = 100;
    private static final int TAKE_PHOTO = 101;

    private Button btnConfirm, btnTakePhoto;
    private TextView txtInsertPicture;
    private ImageView imgInsertPicture;
    private boolean imagePresent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.select_picture);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        this.btnConfirm = (Button)findViewById(R.id.btnConfirm);
        this.btnTakePhoto = (Button)findViewById(R.id.btnTakePhoto);
        this.txtInsertPicture = (TextView)findViewById(R.id.txtInsertPicture);
        this.imgInsertPicture = (ImageView)findViewById(R.id.imgInsertPicture);

        this.txtInsertPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });

        this.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnConfirmOnClick(view);
            }
        });

        this.btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnTakePhotoOnClick(view);
            }
        });

        this.hookWebSocketService();

        this.txtInsertPicture.setHeight(this.txtInsertPicture.getWidth());
        this.imgInsertPicture.setMaxHeight(this.imgInsertPicture.getWidth());

        this.imagePresent = false;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!imagePresent && pictureExists()) {
            File pictureFile = new File(getFilesDir(), Globals.MY_PICTURE_FILE_NAME);
            imgInsertPicture.setImageURI(Uri.fromFile(pictureFile));
            popImageView();
            imagePresent = true;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        service.unregisterCallbacks(WebSocket.Messages.UPLOAD_PICTURE);
    }

    private boolean pictureExists() {
        return Arrays.asList(fileList()).contains(Globals.MY_PICTURE_FILE_NAME);
    }

    private void popImageView() {
        imgInsertPicture.setVisibility(View.VISIBLE);
        txtInsertPicture.setVisibility(View.INVISIBLE);

        txtInsertPicture.setOnClickListener(null);
        imgInsertPicture.setClickable(true);

        imgInsertPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });
    }


    @Override
    protected void setupWebSocketHandlers() {
        final Context context = this;

        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onPictureUploadSuccess();
                    }
                });
            }
        };
        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                final String error = args[0].toString();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onPictureUploadFailure(error);
                    }
                });
            }
        };

        this.service.registerCallbacks(WebSocket.Messages.UPLOAD_PICTURE, successHandler, failureHandler);
    }

    private void onPictureUploadSuccess() {
        File pictureFile = new File(getFilesDir(), Globals.MY_PICTURE_FILE_NAME);
        File b64pictureFile = new File(getFilesDir(), Globals.MY_PICTURE_B64_FILE_NAME);

        Intent dataIntent = new Intent();
        dataIntent.putExtra(INTENT_DATA_PICTURE_FILE, pictureFile);         // Serializable
        dataIntent.putExtra(INTENT_DATA_PICTURE_B64_FILE, b64pictureFile);  // Serializable

        setControlsEnabled(true);

        this.setResult(RESULT_OK, dataIntent);
        this.finish();
    }

    private void onPictureUploadFailure(String error) {
        setControlsEnabled(true);
        ToastHelper.showToastInUiThread(this, error);
    }


    private void btnTakePhotoOnClick(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null)
            startActivityForResult(takePictureIntent, TAKE_PHOTO);
    }

    public void btnConfirmOnClick(View view) {
        if (!this.imagePresent || !this.pictureExists()) {
            this.showToast(R.string.error_no_picture);
            return;
        }

        setControlsEnabled(false);

        File b64pictureFile = new File(getFilesDir(), Globals.MY_PICTURE_B64_FILE_NAME);
        String b64picture = Utils.readFileAsString(b64pictureFile);

        webSocket.emit(WebSocket.Messages.UPLOAD_PICTURE, b64picture);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri selectedImage = null;

            if (requestCode == SELECT_PHOTO) {
                selectedImage = data.getData();
            } else if (requestCode == TAKE_PHOTO) {
                Bundle extras = data.getExtras();
                Bitmap photo = (Bitmap)extras.get("data");

                try {
                    File temp = File.createTempFile("photo", Globals.PROFILE_PICTURE_EXTENSION, getCacheDir());
                    FileOutputStream tempStream = new FileOutputStream(temp);
                    photo.compress(Globals.PROFILE_PICTURE_FILE_FORMAT, Globals.PROFILE_PICTURE_QUALITY, tempStream);
                    selectedImage = Uri.fromFile(temp);
                } catch (IOException ex) {
                    this.showToast(R.string.error_saving_picture);
                    return;
                }
            }

            try {
                Bitmap image = this.decodeAndScaleImage(selectedImage, Globals.PROFILE_PICTURE_SIZE);
                imgInsertPicture.setImageBitmap(image);

                this.imagePresent = true;
                this.saveMyProfilePicture(image);
                this.popImageView();
            } catch (IOException e) {
                e.printStackTrace();
                this.showToast(R.string.error_saving_picture);
            }
        }
    }

    private Bitmap decodeAndScaleImage(Uri selectedImage, int requiredSize) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, options);

        int scaledWidth = options.outWidth, scaledHeight = options.outHeight;
        int scale = 1;

        while (scaledWidth / 2 > requiredSize && scaledHeight / 2 > requiredSize) {
            scaledWidth /= 2;
            scaledHeight /= 2;
            scale *= 2;
        }

        BitmapFactory.Options scaledOptions = new BitmapFactory.Options();
        scaledOptions.inSampleSize = scale;

        Bitmap scaledBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, scaledOptions);
        int rotation = 0;

        String[] orientationColumn = { MediaStore.Images.Media.ORIENTATION };
        Cursor cur = managedQuery(selectedImage, orientationColumn, null, null, null);
        int orientation = -1;

        if (cur != null && cur.moveToFirst()) {
            orientation = cur.getInt(cur.getColumnIndex(orientationColumn[0]));
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(orientation);

        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        return this.centerSquareBitmap(rotatedBitmap);
    }

    private Bitmap centerSquareBitmap(Bitmap source) {
        if (source.getWidth() >= source.getHeight())
            return Bitmap.createBitmap(
                    source,
                    source.getWidth()/2 - source.getHeight()/2,
                    0,
                    source.getHeight(),
                    source.getHeight()
            );

        return Bitmap.createBitmap(
                source,
                0,
                source.getHeight()/2 - source.getWidth()/2,
                source.getWidth(),
                source.getWidth()
        );
    }

    private void saveMyProfilePicture(Bitmap picture) throws IOException {
        FileOutputStream outputStream = openFileOutput(Globals.MY_PICTURE_FILE_NAME, MODE_PRIVATE);
        picture.compress(Globals.PROFILE_PICTURE_FILE_FORMAT, Globals.PROFILE_PICTURE_QUALITY, outputStream);
        outputStream.close();

        ByteArrayOutputStream rawOutputStream = new ByteArrayOutputStream();
        picture.compress(Globals.PROFILE_PICTURE_FILE_FORMAT, Globals.PROFILE_PICTURE_QUALITY, rawOutputStream);

        byte[] rawPictureData = rawOutputStream.toByteArray();
        rawOutputStream.close();

        byte[] base64PictureData = Base64.encode(rawPictureData, Base64.DEFAULT);
        rawPictureData = null;

        FileOutputStream base64OutputStream = openFileOutput(Globals.MY_PICTURE_B64_FILE_NAME, MODE_PRIVATE);
        base64OutputStream.write(base64PictureData);
        base64OutputStream.close();
    }


    public static final String INTENT_DATA_PICTURE_FILE = "picture";
    public static final String INTENT_DATA_PICTURE_B64_FILE = "picture-b64";
}