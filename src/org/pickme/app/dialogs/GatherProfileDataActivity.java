package org.pickme.app.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.*;
import org.pickme.R;
import org.pickme.app.base.BaseActivity;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.utils.Globals;
import org.pickme.utils.ToastHelper;
import org.pickme.utils.Utils;

import java.util.Calendar;

public class GatherProfileDataActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {
    public static final String INTENT_DATA_USERNAME = "username";
    public static final String INTENT_DATA_GENDER = "gender";
    public static final String INTENT_DATA_BIRTHDAY = "birthday";
    public static final String INTENT_DATA_COUNTRY_CODE = "country-code";

    private static final int DATE_DIALOG = 999;

    private Button btnConfirm;
    private EditText txtName;
    private Spinner spinGender;
    private Button btnEditBirthday;
    private TextView txtBirthday;
    private EditText txtCountryCode;

    private Calendar chosenBirthday;
    private boolean checkingUsernameAvailability = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.gather_profile_data);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        this.hookWebSocketService();

        this.btnConfirm = (Button)findViewById(R.id.btnConfirm);
        this.txtName = (EditText)findViewById(R.id.txtName);
        this.spinGender = (Spinner)findViewById(R.id.spinGender);
        this.btnEditBirthday = (Button)findViewById(R.id.btnEditBirthday);
        this.txtBirthday = (TextView)findViewById(R.id.txtBirthday);
        this.txtCountryCode = (EditText)findViewById(R.id.txtCountryCode);

        this.txtCountryCode.setText(this.getUserCountry());

        this.setupSpinner();
        this.setupBirthday();

        this.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnConfirmOnClick(view);
            }
        });
        this.btnEditBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnEditBirthdayOnClick(view);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        this.setControlsEnabled(!checkingUsernameAvailability);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        service.unregisterCallbacks(WebSocket.Messages.CHECK_USERNAME);
    }


    @Override
    protected void setupWebSocketHandlers() {
        final Context context = this;

        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onUsernameAvailable();
                    }
                });
            }
        };
        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onUsernameTaken();
                    }
                });
            }
        };

        this.service.registerCallbacks(WebSocket.Messages.CHECK_USERNAME, successHandler, failureHandler);
    }

    private void setupSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.genders, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinGender.setAdapter(adapter);
    }

    private void setupBirthday() {
        Calendar max = Calendar.getInstance();
        max.add(Calendar.YEAR, -Globals.MIN_AGE);

        chosenBirthday = max;

        txtBirthday.setText(Globals.DATE_FORMAT.format(chosenBirthday.getTime()));
    }



    private void btnEditBirthdayOnClick(View view) {
        this.showDialog(DATE_DIALOG);
    }

    public void btnConfirmOnClick(View view) {
        String username = this.getChosenUsername();
        Calendar birthday = this.getChosenBirthday();
        String countryCode = this.getChosenCountryCode();

        if (username.equals("")) {
            this.showToast(R.string.error_username_empty);
            return;
        }

        if ((username.length() < Globals.MIN_USERNAME_LENGTH) || (username.length() > Globals.MAX_USERNAME_LENGTH)) {
            this.showToast(R.string.error_username_length, Globals.MIN_USERNAME_LENGTH, Globals.MAX_USERNAME_LENGTH);
            return;
        }

        if (countryCode.equals("") || (countryCode.length() != 2)) {
            this.showToast(R.string.error_invalid_country_code);
            return;
        }

        if (Utils.computeAge(birthday) < Globals.MIN_AGE) {
            this.showToast(R.string.error_too_young, Globals.MIN_AGE);
            return;
        }

        this.setControlsEnabled(false);
        this.checkingUsernameAvailability = true;

        btnConfirm.setText(getResources().getString(R.string.checking_username_availability));

        webSocket.emit(WebSocket.Messages.CHECK_USERNAME, username);
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        if (id != DATE_DIALOG)
            return null;

        Calendar max = Calendar.getInstance();
        max.add(Calendar.YEAR, -Globals.MIN_AGE);

        DatePickerDialog dialog =  new DatePickerDialog(this, this, max.get(Calendar.YEAR), max.get(Calendar.MONTH), max.get(Calendar.DAY_OF_MONTH));
        DatePicker dpBirthday = dialog.getDatePicker();

        dpBirthday.setCalendarViewShown(false);
        dpBirthday.setSpinnersShown(true);
        dpBirthday.setMaxDate(max.getTimeInMillis());
        dpBirthday.updateDate(max.get(Calendar.YEAR), max.get(Calendar.MONTH), max.get(Calendar.DAY_OF_MONTH));

        return dialog;
    }

    @Override
    public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
        chosenBirthday.set(selectedYear, selectedMonth, selectedDay);
        txtBirthday.setText(Globals.DATE_FORMAT.format(chosenBirthday.getTime()));
    }


    private void onUsernameTaken() {
        this.checkingUsernameAvailability = false;
        btnConfirm.setText(getResources().getString(R.string.confirm));

        ToastHelper.showToastInUiThread(this, R.string.error_username_taken);
        this.setControlsEnabled(true);
    }

    private void onUsernameAvailable() {
        this.checkingUsernameAvailability = false;
        btnConfirm.setText(getResources().getString(R.string.confirm));

        Intent dataIntent = new Intent();
        dataIntent.putExtra(INTENT_DATA_USERNAME, this.getChosenUsername());
        dataIntent.putExtra(INTENT_DATA_GENDER, this.getSelectedGender());
        dataIntent.putExtra(INTENT_DATA_BIRTHDAY, this.getChosenBirthday()); // Serializable
        dataIntent.putExtra(INTENT_DATA_COUNTRY_CODE, this.getChosenCountryCode());

        this.setResult(RESULT_OK, dataIntent);
        this.finish();
    }


    public String getChosenUsername() {
        return txtName.getText().toString();
    }

    public String getSelectedGender() {
        return spinGender.getSelectedItem().toString().toLowerCase().substring(0, 1);
    }

    public Calendar getChosenBirthday() {
        return chosenBirthday;
    }

    public String getChosenCountryCode() {
        return txtCountryCode.getText().toString().toUpperCase();
    }
}