package org.pickme.app.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.TextView;
import org.pickme.app.Application;
import org.pickme.app.StartActivity;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketService;
import org.pickme.utils.ToastHelper;
import org.pickme.entities.attributes.UserAttributes;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseActivity extends Activity {
    protected static final String FONT_DUNKIN = "Dunkin.ttf";

    private List<View> foundViews = new ArrayList<View>();

    protected WebSocketService service;
    protected WebSocket webSocket;

    private static boolean visible = false;

    public static boolean isVisible() {
        return visible;
    }

    @Override
    public void onStart() {
        super.onStart();
        visible = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        visible = false;
    }


    protected void setFont(TextView target, String fontName) {
        Typeface font = Typeface.createFromAsset(getAssets(), fontName);
        target.setTypeface(font);
    }

    protected void setFont(int resourceID, String fontName) {
        this.setFont((TextView) this.findViewById(resourceID), fontName);
    }

    protected void setTextViewText(int textViewID, int stringID) {
        TextView textView = (TextView)findViewById(textViewID);
        textView.setText(getResources().getString(stringID));
    }


    protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();

        if (info == null)
            return false;

        if (!info.isConnected())
            return false;

        if (!info.isAvailable())
            return false;

        return true;
    }


    protected void showToast(int stringID, Object... args) {
        ToastHelper.showToast(this, stringID, args);
    }

    protected void showToast(String text, Object... args) {
        ToastHelper.showToast(this, text, args);
    }


    protected String getUserCountry() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
            String simCountry = telephonyManager.getSimCountryIso();

            if ((simCountry != null) && (simCountry.length() == 2))
                return simCountry.toUpperCase();

            if (telephonyManager.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) {
                String networkCountry = telephonyManager.getNetworkCountryIso();
                if ((networkCountry != null) && (networkCountry.length() == 2))
                    return networkCountry.toUpperCase();
            }
        }
        catch (Exception e) { }

        return "";
    }


    protected void hookWebSocketService() {
        this.service = WebSocketService.getInstance();

        if (this.service == null) {
            this.finish();
            Application.startStartActivityFrom(this);
            return;
        }

        this.webSocket = service.getWebSocket();
        this.setupWebSocketHandlers();
    }

    protected void setupWebSocketHandlers() { } // To be overridden in derived classes


    protected boolean canLoginAutomatically() {
        SharedPreferences sharedPrefs = UserAttributes.get(this);
        String secret = sharedPrefs.getString(UserAttributes.LOGIN_SECRET, null);
        return (secret != null);
    }


    @Override
    public View findViewById(int id) {
        View result = super.findViewById(id);

        if (result != null)
            this.foundViews.add(result);

        return result;
    }

    protected void setControlsEnabled(boolean enabled) {
        for (View control : foundViews)
            control.setEnabled(enabled);
    }
}