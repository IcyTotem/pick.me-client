package org.pickme.app.base;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseExponActivity extends BaseActivity {
    private static final int LABEL_INDEX = 1;

    private List<Expon> expons = new ArrayList<Expon>();

    public void addExpon(int viewID) {
        LinearLayout root = (LinearLayout)findViewById(viewID);

        if (root == null)
            return;

        final Expon expon = new Expon();
        expon.root = root;
        expon.label = (TextView)root.getChildAt(LABEL_INDEX);
        expon.selected = false;

        expon.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expon.selected) {
                    onExponClicked(view);
                } else {
                    for (Expon otherExpon : expons) {
                        otherExpon.selected = false;
                        otherExpon.label.setVisibility(View.GONE);
                    }

                    expon.selected = true;
                    expon.label.setVisibility(View.VISIBLE);
                }
            }
        });

        this.expons.add(expon);
    }

    public void onExponClicked(View view) {

    }

    public void setExponsEnabled(boolean enabled) {
        for (Expon expon : expons)
            expon.root.setClickable(enabled);
    }

    private static class Expon {
        public LinearLayout root;
        public TextView label;
        public boolean selected;
    }
}
