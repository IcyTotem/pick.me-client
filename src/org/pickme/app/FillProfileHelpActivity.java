package org.pickme.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import org.pickme.R;
import org.pickme.app.base.BaseActivity;
import org.pickme.app.dialogs.EditProfileDataActivity;
import org.pickme.app.dialogs.GatherUserPreferenceActivity;
import org.pickme.app.dialogs.SelectPictureActivity;
import org.pickme.utils.Globals;
import org.pickme.entities.attributes.UserAttributes;

import java.io.File;

public class FillProfileHelpActivity extends BaseActivity {
    private static final int SELECT_PICTURE = 1;
    private static final int PICK_DESCRIPTION = 2;
    private static final int SELECT_PREFERENCES = 3;

    private Button btnSelectPicture, btnPickDescription, btnSelectPreferences, btnConfirm;
    private View layoutPictureOk, layoutDescriptionOk, layoutPreferencesOk;
    private boolean pictureOk, descriptionOk, preferencesOk;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.fill_profile_help);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        this.btnSelectPicture = (Button)findViewById(R.id.btnSelectPicture);
        this.btnPickDescription = (Button)findViewById(R.id.btnPickDescription);
        this.btnSelectPreferences = (Button)findViewById(R.id.btnSelectPreferences);
        this.btnConfirm = (Button)findViewById(R.id.btnConfirm);
        this.layoutPictureOk = findViewById(R.id.layoutPictureOk);
        this.layoutDescriptionOk = findViewById(R.id.layoutDescriptionOk);
        this.layoutPreferencesOk = findViewById(R.id.layoutPreferencesOk);

        this.pictureOk = false;
        this.descriptionOk = false;
        this.preferencesOk = false;

        this.btnSelectPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSelectPictureOnClick(view);
            }
        });
        this.btnPickDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnPickDescriptionOnClick(view);
            }
        });
        this.btnSelectPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSelectPreferencesOnClick(view);
            }
        });
        this.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnConfirmOnClick(view);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        SharedPreferences prefs = UserAttributes.get(this);

        this.pictureOk = (new File(getFilesDir(), Globals.MY_PICTURE_FILE_NAME)).exists();
        this.descriptionOk = !prefs.getString(UserAttributes.DESCRIPTION, "null").equals("null");
        this.preferencesOk = prefs.getBoolean(UserAttributes.LOOKING_FOR_COMPLETED, false);

        this.setupElementsVisibility();
    }

    private void setupElementsVisibility() {
        btnSelectPicture.setVisibility(pictureOk ? View.GONE : View.VISIBLE);
        layoutPictureOk.setVisibility(pictureOk ? View.VISIBLE : View.GONE);

        btnPickDescription.setVisibility(descriptionOk ? View.GONE : View.VISIBLE);
        layoutDescriptionOk.setVisibility(descriptionOk ? View.VISIBLE : View.GONE);

        btnSelectPreferences.setVisibility(preferencesOk ? View.GONE : View.VISIBLE);
        layoutPreferencesOk.setVisibility(preferencesOk ? View.VISIBLE : View.GONE);

        btnConfirm.setEnabled(pictureOk && descriptionOk && preferencesOk);
    }


    private void btnSelectPictureOnClick(View view) {
        Intent intent = new Intent(getApplicationContext(), SelectPictureActivity.class);
        startActivityForResult(intent, SELECT_PICTURE);
    }

    private void btnPickDescriptionOnClick(View view) {
        Intent intent = new Intent(getApplicationContext(), EditProfileDataActivity.class);
        startActivityForResult(intent, PICK_DESCRIPTION);
    }

    private void btnSelectPreferencesOnClick(View view) {
        Intent intent = new Intent(getApplicationContext(), GatherUserPreferenceActivity.class);
        startActivityForResult(intent, SELECT_PREFERENCES);
    }

    private void btnConfirmOnClick(View view) {
        SharedPreferences.Editor editor = UserAttributes.edit(this);
        editor.putBoolean(UserAttributes.FIRST_LOGIN, false);
        editor.apply();

        this.finish();

        Application.startMainActivityFrom(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case SELECT_PICTURE:
                this.pictureOk = true;
                break;

            case PICK_DESCRIPTION:
                this.descriptionOk = true;
                break;

            case SELECT_PREFERENCES:
                this.preferencesOk = true;
                break;
        }

        this.setupElementsVisibility();
    }
}