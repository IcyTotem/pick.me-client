package org.pickme.app.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.pickme.R;
import org.pickme.entities.Message;
import org.pickme.service.PictureLoadingService;
import org.pickme.utils.Globals;
import org.pickme.utils.Utils;

import java.io.File;
import java.util.Calendar;
import java.util.List;

public class MessageAdapter extends ArrayAdapter {
    private String headerFormat, you;
    private PictureLoadingService pictureLoadingService;
    private int messageReadColor;

    public MessageAdapter(Context context, List objects) {
        super(context, R.layout.message_list_item, objects);

        this.pictureLoadingService = PictureLoadingService.getInstance();

        this.headerFormat = context.getString(R.string.chat_message_header);
        this.you = context.getString(R.string.you);
        this.messageReadColor = context.getResources().getColor(R.color.message_read);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Message message = (Message)this.getItem(position);
        LayoutInflater inflater = LayoutInflater.from(getContext());

        if (message.isMine())
            convertView = inflater.inflate(R.layout.message_me_list_item, parent, false);
        else
            convertView = inflater.inflate(R.layout.message_list_item, parent, false);


        ViewHolder viewHolder = this.getViewHolder(convertView);
        this.embedDataIntoView(position, viewHolder);

        return convertView;
    }

    private ViewHolder getViewHolder(View convertView) {
        if (convertView.getTag() != null)
            return (ViewHolder)convertView.getTag();

        ViewHolder viewHolder = this.createViewHolder(convertView);
        convertView.setTag(viewHolder);

        return viewHolder;

    }

    private ViewHolder createViewHolder(View convertView) {
        ViewHolder viewHolder = new ViewHolder();

        viewHolder.txtHeader = (TextView)convertView.findViewById(R.id.txtHeader);
        viewHolder.txtMessage = (TextView)convertView.findViewById(R.id.txtMessage);
        viewHolder.imgProfile = (ImageView)convertView.findViewById(R.id.imgProfile);
        viewHolder.whole = (LinearLayout)convertView.findViewById(R.id.wholeItem);

        return viewHolder;
    }

    private void embedDataIntoView(int position, ViewHolder viewHolder) {
        Message message = (Message)this.getItem(position);
        String senderName = message.isMine() ? you : message.getSender().getName();
        Calendar sendDate = message.getSendDate();
        String header;

        if (Utils.isToday(sendDate))
            header = String.format(headerFormat, Globals.TIME_FORMAT.format(sendDate.getTime()), senderName);
        else
            header = String.format(headerFormat, Globals.DATE_TIME_FORMAT.format(sendDate.getTime()), senderName);

        viewHolder.txtHeader.setText(header);
        viewHolder.txtMessage.setText(message.getText());

        if (message.isMine())
            pictureLoadingService.loadMyPicture(viewHolder.imgProfile);
        else
            pictureLoadingService.loadPicture(message.getSender(), viewHolder.imgProfile);

        if (message.isMine() && message.isRead())
            viewHolder.whole.setBackgroundColor(messageReadColor);
    }


    private static class ViewHolder {
        public TextView txtHeader, txtMessage;
        public ImageView imgProfile;
        public LinearLayout whole;
    }
}
