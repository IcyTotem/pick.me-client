package org.pickme.app.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import org.pickme.R;
import org.pickme.entities.User;
import org.pickme.service.PictureLoadingService;

import java.util.List;

public class UserAdapter extends ArrayAdapter {
    private Drawable defaultPicture;
    private String emptyDescription, updatedAgoFormat;
    private int usedLayout;

    private PictureLoadingService pictureLoadingService;


    public UserAdapter(Context context, List objects, int layout) {
        super(context, layout, objects);

        this.usedLayout = layout;

        Resources resources = context.getResources();

        this.defaultPicture = resources.getDrawable(R.drawable.person);
        this.emptyDescription = resources.getString(R.string.user_empty_description);
        this.updatedAgoFormat = resources.getString(R.string.updated_x_ago);

        pictureLoadingService = PictureLoadingService.getInstance();
    }

    public UserAdapter(Context context, List objects) {
        this(context, objects, R.layout.user_list_item);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(usedLayout, parent, false);
        }

        ViewHolder viewHolder = this.getViewHolder(convertView);
        this.embedDataIntoView(position, viewHolder);

        return convertView;
    }

    private ViewHolder getViewHolder(View convertView) {
        if (convertView.getTag() != null)
            return (ViewHolder)convertView.getTag();

        ViewHolder viewHolder = this.createViewHolder(convertView);
        convertView.setTag(viewHolder);

        return viewHolder;

    }


    protected ViewHolder createViewHolder(View convertView) {
        ViewHolder viewHolder = this.createViewHolder();

        viewHolder.txtHeader = (TextView)convertView.findViewById(R.id.txtHeader);
        viewHolder.txtSubheader1 = (TextView)convertView.findViewById(R.id.txtSubheader1);
        viewHolder.txtSubheader2 = (TextView)convertView.findViewById(R.id.txtSubheader2);
        viewHolder.imgProfile = (ImageView)convertView.findViewById(R.id.imgProfile);

        return viewHolder;
    }

    protected ViewHolder createViewHolder() {
        return new ViewHolder();
    }

    protected void embedDataIntoView(int position, ViewHolder viewHolder) {
        User user = (User)this.getItem(position);
        String header, subheader1, subheader2;

        if (user.hasBirthday())
            header = String.format("%s, %d", user.getName(), user.getAge());
        else
            header = user.getName();

        if (user.hasDescription())
            subheader1 = user.getDescription();
        else
            subheader1 = this.emptyDescription;

        if (user.hasLastUpdate())
            subheader2 = String.format("%dm, %s", user.getRange(), String.format(this.updatedAgoFormat, user.getPeriodSinceLastUpdate()));
        else
            subheader2 = String.format("%dm", user.getRange());

        viewHolder.txtHeader.setText(header);
        viewHolder.txtSubheader1.setText(subheader1);
        viewHolder.txtSubheader2.setText(subheader2);

        pictureLoadingService.loadPicture(user, viewHolder.imgProfile);
    }


    protected static class ViewHolder {
        public TextView txtHeader, txtSubheader1, txtSubheader2;
        public ImageView imgProfile;
    }
}
