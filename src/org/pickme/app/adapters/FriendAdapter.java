package org.pickme.app.adapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import org.pickme.R;
import org.pickme.entities.Friend;

import java.util.List;

public class FriendAdapter extends UserAdapter {
    public FriendAdapter(Context context, List objects) {
        super(context, objects, R.layout.friend_list_item);
    }

    @Override
    protected ViewHolder createViewHolder() {
        return new FriendViewHolder();
    }

    @Override
    protected ViewHolder createViewHolder(View convertView) {
        FriendViewHolder viewHolder = (FriendViewHolder)super.createViewHolder(convertView);
        viewHolder.txtUnreadMessages = (TextView)convertView.findViewById(R.id.txtUnreadMessages);
        return viewHolder;
    }


    protected void embedDataIntoView(int position, ViewHolder viewHolder) {
        super.embedDataIntoView(position, viewHolder);

        Friend friend = (Friend)this.getItem(position);
        FriendViewHolder friendViewHolder = (FriendViewHolder)viewHolder;

        friendViewHolder.txtUnreadMessages.setText(String.valueOf(friend.getUnreadMessagesCount()));
        friendViewHolder.txtUnreadMessages.setVisibility(friend.getUnreadMessagesCount() == 0 ? View.GONE : View.VISIBLE);
    }


    protected static class FriendViewHolder extends ViewHolder {
        public TextView txtUnreadMessages;
    }
}
