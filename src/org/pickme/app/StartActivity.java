package org.pickme.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import org.pickme.R;
import org.pickme.app.base.BaseActivity;
import org.pickme.service.*;
import org.pickme.utils.ToastHelper;
import org.pickme.entities.attributes.UserAttributes;

public class StartActivity extends BaseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setContentView(R.layout.start);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        if (this.isNetworkAvailable()) {
            this.setupWebSocketServiceCallbacks();
            this.startWebSocketService();
        } else
            this.setTextViewText(R.id.txtError, R.string.error_no_network);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!this.isNetworkAvailable()) {
            this.setTextViewText(R.id.txtError, R.string.error_no_network);
            return;
        }

        WebSocketService webSocketService = WebSocketService.getInstance();

        if (webSocketService == null) {
            this.setupWebSocketServiceCallbacks();
            this.startWebSocketService();
        } else if (!webSocketService.isConnected()) {
            setTextViewText(R.id.txtError, R.string.reconnecting);
            webSocketService.retryConnect();
        } else if (this.canLoginAutomatically()) {
            Intent intent = new Intent(getApplicationContext(), LoginService.class);
            intent.putExtra(LoginService.LOGIN_AUTOMATICALLY, true);
            startService(intent);
        } else {
            Application.startStartMenuActivityFrom(this);
        }
    }

    private void setupWebSocketServiceCallbacks() {
        // Run once the websocket service is initialized (after startService has been called)
        WebSocketService.runWhenReady(new Runnable() {
            @Override
            public void run() {
                WebSocketService service = WebSocketService.getInstance();

                // Rune once the websocket is connected (after WebSocket.onConnect has been raised)
                service.runWhenConnected(new Runnable() {
                    @Override
                    public void run() {
                        // If login data (secret and device-id) are available, we can login automatically as soon as the
                        // websocket is connected to the remote server
                        boolean autoLogin = canLoginAutomatically();

                        // If the autologin is enabled, the login service will attempt to automatically log the user in
                        // and in case of success or failure, something will have to happen: this code states what will
                        // need to happen in such cases
                        if (autoLogin) {
                            LoginService.runWhenReady(new Runnable() {
                                @Override
                                public void run() {
                                    final LoginService loginService = LoginService.getInstance();
                                    loginService.runAfterLogin(new Runnable() {
                                        @Override
                                        public void run() {
                                            // If this is the first login, helps the user complete his profile
                                            SharedPreferences prefs = UserAttributes.get(getApplicationContext());
                                            if (prefs.getBoolean(UserAttributes.FIRST_LOGIN, true))
                                                startActivity(new Intent(getApplicationContext(), FillProfileHelpActivity.class));
                                            else
                                                Application.startMainActivityFrom(StartActivity.this);
                                        }
                                    });
                                    loginService.runOnLoginFailure(new Runnable() {
                                        @Override
                                        public void run() {
                                            // If login fails for whichever reason, go to the main menu
                                            Intent menuIntent = new Intent(getApplicationContext(), StartMenuActivity.class);
                                            menuIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(menuIntent);
                                            ToastHelper.showToastInUiThread(loginService, loginService.getLastError());
                                        }
                                    });
                                }
                            });
                        }

                        // Starts the login service
                        Intent loginIntent = new Intent(getApplicationContext(), LoginService.class);
                        loginIntent.putExtra(LoginService.LOGIN_AUTOMATICALLY, autoLogin);
                        startService(loginIntent);

                        // If no automatic login can be performed, start the normal main menu
                        if (!autoLogin)
                            Application.startStartMenuActivityFrom(StartActivity.this);
                    }
                });
            }
        });
    }

    private void startWebSocketService() {
        this.startService(new Intent(getApplicationContext(), WebSocketService.class));
    }
}