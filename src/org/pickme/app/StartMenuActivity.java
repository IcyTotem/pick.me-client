package org.pickme.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import org.pickme.R;
import org.pickme.app.base.BaseExponActivity;
import org.pickme.service.LoginService;
import org.pickme.service.WebSocketService;

public class StartMenuActivity extends BaseExponActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.start_menu);
        this.setFont(R.id.txtTitle, FONT_DUNKIN);

        this.addExpon(R.id.exponCreateProfile);
        this.addExpon(R.id.exponLogin);
        this.addExpon(R.id.exponTroubleshooting);
        this.addExpon(R.id.exponQuit);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (LoginService.isAvailable() && LoginService.getInstance().isLogged())
            Application.startMainActivityFrom(this);
    }

    @Override
    public void onExponClicked(View view) {
        switch (view.getId()) {
            case R.id.exponCreateProfile:
                Intent createProfileIntent = new Intent(getApplicationContext(), CreateProfileActivity.class);
                createProfileIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(createProfileIntent);
                break;

            case R.id.exponLogin:
                Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginIntent);
                break;

            case R.id.exponTroubleshooting:
                startActivity(new Intent(getApplicationContext(), TroubleshootingActivity.class));
                break;

            case R.id.exponQuit:
                WebSocketService.getInstance().stopSelf();

                if (LoginService.isAvailable())
                    LoginService.getInstance().stopSelf();

                StartMenuActivity.this.finish();
        }
    }
}