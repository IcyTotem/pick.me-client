package org.pickme.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import org.json.JSONException;
import org.json.JSONObject;
import org.pickme.R;
import org.pickme.app.base.BaseFragmentActivity;
import org.pickme.app.fragments.MessagesListFragment;
import org.pickme.entities.Friend;
import org.pickme.entities.Message;
import org.pickme.service.WebSocket;
import org.pickme.service.WebSocketEventHandler;
import org.pickme.service.WebSocketService;
import org.pickme.entities.attributes.MessageAttributes;
import org.pickme.utils.ToastHelper;

public class ChatActivity extends BaseFragmentActivity {
    private static final String TAG = ChatActivity.class.getSimpleName();

    public static final String INTENT_TARGET = "intent-target";

    private static ChatActivity instance;

    private WebSocketService webSocketService;
    private WebSocket webSocket;

    private MessagesListFragment messagesFragment;
    private EditText txtMessage;
    private ImageButton btnSend;

    private Friend target;
    private static boolean visible = false;
    private static Friend currentTarget = null;


    public static Friend getCurrentTarget() {
        return currentTarget;
    }

    public static boolean isVisible() {
        return visible;
    }

    public static ChatActivity getInstance() {
        return instance;
    }


    public ChatActivity() {
        ChatActivity.instance = this;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.chat);

        this.webSocketService = WebSocketService.getInstance();
        this.webSocket = webSocketService.getWebSocket();
        this.setupWebSocketHandlers();

        this.messagesFragment = new MessagesListFragment();

        if (savedInstanceState == null)
            this.getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, messagesFragment).commit();

        this.txtMessage = (EditText)findViewById(R.id.txtMessage);
        this.btnSend = (ImageButton)findViewById(R.id.btnSend);

        this.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSendOnClick(view);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        this.target = (Friend)getIntent().getParcelableExtra(INTENT_TARGET);
        this.messagesFragment.setTarget(target);

        ChatActivity.visible = true;
        ChatActivity.currentTarget = target;
    }

    @Override
    public void onStop() {
        super.onStop();
        ChatActivity.visible = false;
        ChatActivity.currentTarget = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        webSocketService.unregisterCallbacks(WebSocket.Messages.SEND_MESSAGE);
    }

    private void setupWebSocketHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                JSONObject data = (JSONObject)args[0];
                onSendMessageSuccess(data);
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String error = args[0].toString();
                Log.i(TAG, error);
                messagesFragment.removeMyLastMessages();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastHelper.showToastInUiThread(getApplicationContext(), R.string.error_chat_message_not_sent);
                    }
                });
            }
        };

        webSocketService.registerCallbacks(WebSocket.Messages.SEND_MESSAGE, successHandler, failureHandler);
    }


    private void btnSendOnClick(View view) {
        String text = txtMessage.getText().toString();

        if (text.length() == 0) {
            ToastHelper.showToast(this, R.string.error_chat_message_empty);
            return;
        }

        ComposedMessage composedMessage = new ComposedMessage();
        composedMessage.setRecipientID(target.getID());
        composedMessage.setText(text);

        // The problem is that when I send a message, I don't know what its ID will be. By attaching an hashcode to
        // the sent data, when I receive a success ack, I can select the message with the given hash code and set its ID
        Message displayedMessage = messagesFragment.addMyMessage(text);
        composedMessage.setHashCode(displayedMessage.hashCode());

        webSocket.emit(WebSocket.Messages.SEND_MESSAGE, composedMessage);
        txtMessage.setText("");
    }

    private void onSendMessageSuccess(JSONObject data) {
        try {
            int hashCode, id;

            hashCode = data.getInt(MessageAttributes.HASH_CODE);
            id = data.getInt(MessageAttributes.ID);

            messagesFragment.setMyMessageID(hashCode, id);
        } catch (JSONException e) { }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chat, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuLast200Messages:
                messagesFragment.loadRecentMessages(200);
                return true;

            case R.id.menuAllMessages:
                messagesFragment.loadAllMessages();
                return true;

            case R.id.menuFetchMessages:
                messagesFragment.fetchMessages();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private static class ComposedMessage {
        private int recipientID, hashCode;
        private String text;

        public int getRecipientID() {
            return recipientID;
        }

        public void setRecipientID(int recipientID) {
            this.recipientID = recipientID;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getHashCode() {
            return hashCode;
        }

        public void setHashCode(int hashCode) {
            this.hashCode = hashCode;
        }
    }
}