package org.pickme.utils;

import android.graphics.Bitmap;

import java.text.SimpleDateFormat;

public class Globals {
    public static final int MIN_AGE = 16;
    public static final int MIN_USERNAME_LENGTH = 5;
    public static final int MAX_USERNAME_LENGTH = 15;
    public static final int MIN_RANGE = 50;
    public static final int MAX_RANGE = 3000;
    public static final int MIN_PASSWORD_LENGTH = 7;
    public static final int MIN_REFRESH_PERIOD = 1;
    public static final int MAX_REFRESH_PERIOD = 30;

    public static final float LOW_BATTERY_THRESHOLD = 0.35f;
    public static final float VERY_LOW_BATTERY_THRESHOLD = 0.20f;
    public static final float VERY_LOW_BATTERY_LOCATION_DAMP_FACTOR = 2.0f;

    public static final String MY_PICTURE_FILE_NAME = "me.jpg";
    public static final String MY_PICTURE_B64_FILE_NAME = "me.jpg.base64";
    public static final String PROFILE_PICTURE_EXTENSION = ".jpg";
    public static final String PROFILE_PICTURE_NAME_FORMAT = "picture-%d";
    public static final int PROFILE_PICTURE_QUALITY = 90;
    public static final int PROFILE_PICTURE_SIZE = 285;
    public static final Bitmap.CompressFormat PROFILE_PICTURE_FILE_FORMAT = Bitmap.CompressFormat.JPEG;

    public static final String MESSAGE_HISTORY_FILE_NAME_FORMAT = "message-history-%d";
    public static final String MESSAGE_HISTORY_FILE_EXTENSION = ".txt";
    public static final int DEFAULT_RECENT_MESSAGES_COUNT = 50;

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat DATE_TIME_FORMAT  =  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("hh:mm:ss");
}
