package org.pickme.utils;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

public class ToastHelper {
    private static final int TEXT_LONG_THRESHOLD = 40; // characters count

    public static void showToastInUiThread(final Context context, final int stringID, final Object... args) {
        Handler handler = new Handler(context.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                showToast(context, stringID, args);
            }
        });
    }

    public static void showToastInUiThread(final Context context, final String text, final Object... args) {
        Handler handler = new Handler(context.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                showToast(context, text, args);
            }
        });
    }

    public static void showToast(Context context, int stringID, Object... args) {
        String text = String.format(context.getResources().getString(stringID), args);
        showToast(context, text);
    }

    public static void showToast(Context context, String text, Object... args) {
        if (text == null)
            return;

        String resource = getStringResourceByName(context, text);

        if (resource != null)
            text = resource;

        showToast(context, String.format(text, args));
    }

    private static String getStringResourceByName(Context context, String resourceName) {
        if (resourceName == null)
            return null;

        String packageName = context.getPackageName();
        int resID = context.getResources().getIdentifier(resourceName, "string", packageName);

        if (resID != 0)
            return context.getString(resID);
        else
            return null;
    }

    private static void showToast(Context context, String text) {
        if (text.length() > TEXT_LONG_THRESHOLD)
            Toast.makeText(context, text, Toast.LENGTH_LONG).show();
        else
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}
