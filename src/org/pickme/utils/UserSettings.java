package org.pickme.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;

public class UserSettings {
    public static final String LOCATION_PROVIDER = "LocationProvider";
    public static final String LOCATION_UPDATE_PERIOD = "MinUpdatePeriod";
    public static final String LOCATION_UPDATE_DISTANCE = "MinUpdateDistance";
    public static final String REQUEST_UPDATE_PERIOD = "MinRequestUpdatePeriod";
    public static final String FRIEND_UPDATE_PERIOD = "MinFriendUpdatePeriod";

    public static final String DEFAULT_LOCATION_PROVIDER = LocationManager.GPS_PROVIDER;
    public static final long DEFAULT_LOCATION_UPDATE_PERIOD = 5 * 60 * 1000; // 5 MINUTES
    public static final long DEFAULT_LOCATION_UPDATE_DISTANCE = 50; // 50 METERS
    public static final long DEFAULT_REQUEST_UPDATE_PERIOD = 5 * 60 * 1000; // 5 MINUTES
    public static final long DEFAULT_FRIEND_UPDATE_PERIOD = 5 * 60 * 1000; // 5 MINUTES

    private static final String LOCAL_USER_ATTRIBUTES = "local-user-settings";

    public static SharedPreferences get(Context context) {
        return context.getSharedPreferences(LOCAL_USER_ATTRIBUTES, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor edit(Context context) {
        return get(context).edit();
    }
}
