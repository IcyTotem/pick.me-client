package org.pickme.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Utils {
    public static int computeAge(Calendar birthday) {
        Calendar now = Calendar.getInstance();
        int diff = now.get(Calendar.YEAR) - birthday.get(Calendar.YEAR);

        if ((birthday.get(Calendar.MONTH) > now.get(Calendar.MONTH)) ||
            (birthday.get(Calendar.MONTH) == now.get(Calendar.MONTH) && birthday.get(Calendar.DATE) > now.get(Calendar.DATE)))
            diff--;

        return diff;
    }

    public static int differenceInMinutes(Calendar d1, Calendar d2) {
        long millis1 = d1.getTimeInMillis();
        long millis2 = d2.getTimeInMillis();
        return (int)Math.abs((millis1 - millis2) / 60000);
    }

    public static String readFileAsString(File file) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            StringBuilder builder = new StringBuilder();
            String ls = System.getProperty("line.separator");

            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append(ls);
            }

            reader.close();

            return builder.toString();
        } catch (IOException ex) {
            return null;
        }
    }

    public static boolean isToday(Calendar date) {
        Calendar today = Calendar.getInstance();
        return (date.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH)) &&
               (date.get(Calendar.MONTH) == today.get(Calendar.MONTH)) &&
               (date.get(Calendar.YEAR) == today.get(Calendar.YEAR));
    }
}
