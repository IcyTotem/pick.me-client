package org.pickme.service;

public interface WebSocket {
    void emit(String event, Object... args);
    void disconnect();

    public class Messages {
        public static final String CHECK_USERNAME = "check username";
        public static final String CREATE_USER = "create user";
        public static final String LOGIN = "login";
        public static final String UPDATE_POSITION = "update position";
        public static final String UPDATE_PREFERENCE = "update preference";
        public static final String UPLOAD_PICTURE = "upload picture";
        public static final String UPDATE_PROFILE = "update profile";
        public static final String GET_NEARBY_USERS = "get nearby users";
        public static final String GET_PICTURE = "get picture";
        public static final String SEND_REQUEST = "send request";
        public static final String GET_INCOMING_REQUESTS = "get incoming requests";
        public static final String ACCEPT_REQUEST = "accept request";
        public static final String DECLINE_REQUEST = "decline request";
        public static final String REQUEST_RECEIVED = "request received";
        public static final String GET_NOTIFICATIONS = "get notifications";
        public static final String GET_FRIENDS = "get friends";
        public static final String GET_UNREAD_MESSAGES_COUNT_BY_SENDER = "get unread messages counts by sender";
        public static final String MESSAGE_RECEIVED = "message received";
        public static final String MARK_MESSAGE_AS_READ = "mark message as read";
        public static final String SEND_MESSAGE = "send message";
        public static final String GET_ALL_MESSAGES_FROM = "get all messages from";
        public static final String GET_UNREAD_MESSAGES_FROM = "get unread messages from";
        public static final String BLACKLIST = "blacklist";
        public static final String READ_MESSAGES_FROM = "read messages from";
        public static final String LOGOUT = "logout";
        public static final String UNFRIEND = "unfriend";
        public static final String UNFRIENDED = "unfriended";
        public static final String BLACKLISTED = "blacklisted";
        public static final String SWITCH_DEVICE = "switch device";
        public static final String BLOCK = "block";
        public static final String BLOCKED = "blocked";
        public static final String CHANGE_PASSWORD = "change password";
        public static final String REQUEST_ACCEPTED = "request accepted";
        public static final String MESSAGE_READ = "message read";
        public static final String NOT_LOGGED = "not logged";
        public static final String ALL_MESSAGES_READ_BY = "all messages read by";
    }
}
