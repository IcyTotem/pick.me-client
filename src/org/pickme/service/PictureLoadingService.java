package org.pickme.service;

import android.app.Service;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.IBinder;
import android.widget.ImageView;
import org.json.JSONException;
import org.json.JSONObject;
import org.pickme.R;
import org.pickme.entities.User;
import org.pickme.utils.Globals;
import org.pickme.entities.attributes.UserAttributes;

import java.io.File;
import java.util.*;

public class PictureLoadingService extends Service {
    private static final int PICTURES_COUNT_THRESHOLD = 140;

    private static PictureLoadingService instance;

    private WebSocketService webSocketService;

    private String lastError;
    private Map<Integer, Container> containersMap;
    private List<File> pictures;
    private Drawable defaultPicture;
    private File myPictureFile;


    public static PictureLoadingService getInstance() {
        return instance;
    }

    public static boolean isAvailable() {
        return (instance != null);
    }

    public String getLastError() {
        return lastError;
    }

    private void setLastError(String lastError) {
        this.lastError = lastError;
    }


    public PictureLoadingService() {
        this.containersMap = new HashMap<Integer, Container>();
        PictureLoadingService.instance = this;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if ((this.webSocketService = WebSocketService.getInstance()) != null)
            this.setupWebSocketHandlers();

        this.defaultPicture = getApplicationContext().getResources().getDrawable(R.drawable.person);
        this.myPictureFile = new File(getFilesDir(), Globals.MY_PICTURE_FILE_NAME);

        this.enumeratePictures();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (this.webSocketService != null)
            this.webSocketService.unregisterCallbacks(WebSocket.Messages.GET_PICTURE);

        PictureLoadingService.instance = null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!webSocketService.hasCallbacks(WebSocket.Messages.GET_PICTURE))
            this.setupWebSocketHandlers();

        return START_STICKY;
    }

    private void setupWebSocketHandlers() {
        final WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String error = args[0].toString();
                setLastError(error);
            }
        };

        final WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                JSONObject result = (JSONObject)args[0];
                onGetPictureSuccess(result);
            }
        };

        this.webSocketService.registerCallbacks(WebSocket.Messages.GET_PICTURE, successHandler, failureHandler);
    }


    public void loadMyPicture(ImageView view) {
        if (myPictureFile.exists())
            view.setImageURI(Uri.fromFile(myPictureFile));
        else
            view.setImageDrawable(defaultPicture);
    }

    public void loadPicture(User owner, ImageView view) {
        if (owner.hasPictureFile()) {
            view.setImageURI(Uri.fromFile(owner.getPictureFile()));
            return;
        }

        if (containersMap.containsKey(owner.getID()))
            return;

        Container container = new Container();
        container.owner = owner;
        container.view = view;
        containersMap.put(owner.getID(), container);

        WebSocket webSocket = webSocketService.getWebSocket();
        webSocket.emit(WebSocket.Messages.GET_PICTURE, owner.getID());
    }

    private void onGetPictureSuccess(JSONObject data) {
        String base64Picture;
        Integer userID;

        try {
            base64Picture = data.getString(UserAttributes.PICTURE);
            userID = data.getInt(UserAttributes.ID);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        final Container container = containersMap.get(userID);

        if (container.view == null)
            return;

        if ((base64Picture == null) || (base64Picture.length() == 0) || base64Picture.equals("null")) {
            container.view.post(new Runnable() {
                @Override
                public void run() {
                    container.view.setImageDrawable(defaultPicture);
                }
            });
            return;
        }

        container.owner.setPictureFile(getApplicationContext(), base64Picture);
        container.view.post(new Runnable() {
            @Override
            public void run() {
                container.view.setImageURI(Uri.fromFile(container.owner.getPictureFile()));
            }
        });

        containersMap.remove(userID);

        this.pictures.add(container.owner.getPictureFile());

        if (pictures.size() > PICTURES_COUNT_THRESHOLD)
            this.deleteOldPictures();
    }

    private void enumeratePictures() {
        String[] fileNames = getApplicationContext().fileList();
        File base = getApplicationContext().getFilesDir();

        this.pictures = new ArrayList<File>();

        for (String fileName : fileNames)
            if (fileName.startsWith("picture"))
                this.pictures.add(new File(base, fileName));
    }

    private void deleteOldPictures() {
        Collections.sort(pictures, FileComparator.getInstance());

        while (pictures.size() > PICTURES_COUNT_THRESHOLD) {
            File image = pictures.get(0);
            image.delete();
            pictures.remove(0);
        }
    }


    private static class Container {
        public ImageView view;
        public User owner;
    }

    private static class FileComparator implements Comparator<File> {
        private static FileComparator instance;

        public static FileComparator getInstance() {
            return (instance != null) ? instance : (instance = new FileComparator());
        }

        @Override
        public int compare(File file, File file2) {
            return (file.lastModified() - file2.lastModified() > 0) ? 1 : 0;
        }
    }
}
