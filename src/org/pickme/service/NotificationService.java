package org.pickme.service;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.WindowManager;
import org.json.JSONException;
import org.json.JSONObject;
import org.pickme.R;
import org.pickme.app.Application;
import org.pickme.app.ChatActivity;
import org.pickme.app.MainActivity;
import org.pickme.app.dialogs.EditProfileDataActivity;
import org.pickme.app.dialogs.EditSettingsActivity;
import org.pickme.app.dialogs.SelectPictureActivity;
import org.pickme.entities.attributes.BlacklistAttributes;
import org.pickme.entities.attributes.MessageAttributes;
import org.pickme.entities.attributes.NotificationAttributes;

public class NotificationService extends Service {
    private static final String TAG = NotificationService.class.getSimpleName();

    private static final int REQUEST_RECEIVED_NOTIFICATION_ID = 0;
    private static final int MESSAGE_RECEIVED_NOTIFICATION_ID = 50;
    private static final int SUMMARY_NOTIFICATION_ID = 100;
    private static final int UNFRIENDED_NOTIFICATION_ID = 150;
    private static final int BLACKLISTED_NOTIFICATION_ID = 200;
    private static final int BLOCKED_NOTIFICATION_ID = 250;
    private static final int REQUEST_ACCEPTED_NOTIFICATION_ID = 300;

    private static NotificationService instance;

    private WebSocketService webSocketService;
    private WebSocketEventHandler messageReceivedHandler;

    public static NotificationService getInstance() {
        return instance;
    }

    public static boolean isAvailable() {
        return (instance != null);
    }


    public NotificationService() {
        NotificationService.instance = this;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if ((this.webSocketService = WebSocketService.getInstance()) != null)
            this.setupWebSocketHandlers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (this.webSocketService != null) {
            webSocketService.unregisterHandlers(WebSocket.Messages.REQUEST_RECEIVED);
            webSocketService.unregisterCallbacks(WebSocket.Messages.GET_NOTIFICATIONS);
            webSocketService.unregisterHandler(WebSocket.Messages.MESSAGE_RECEIVED, messageReceivedHandler);
            webSocketService.unregisterHandlers(WebSocket.Messages.UNFRIENDED);
            webSocketService.unregisterHandlers(WebSocket.Messages.BLACKLISTED);
            webSocketService.unregisterHandlers(WebSocket.Messages.BLOCKED);
        }

        NotificationService.instance = null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!webSocketService.hasCallbacks(WebSocket.Messages.REQUEST_RECEIVED))
            this.setupRequestReceivedHandler();

        if (!webSocketService.hasCallbacks(WebSocket.Messages.GET_NOTIFICATIONS))
            this.setupGetNotificationsHandlers();

        WebSocket webSocket = webSocketService.getWebSocket();
        webSocket.emit(WebSocket.Messages.GET_NOTIFICATIONS);

        return START_STICKY;
    }

    private void setupWebSocketHandlers() {
        this.setupRequestReceivedHandler();
        this.setupMessageReceivedHandler();
        this.setupGetNotificationsHandlers();
        this.setupUnfriendedHandler();
        this.setupBlacklistedHandler();
        this.setupBlockedHandler();
        this.setupRequestAcceptedHandler();
    }

    private void setupRequestReceivedHandler() {
        final WebSocketEventHandler handler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String senderName = args[0].toString();
                onRequestReceived(senderName);
            }
        };

        this.webSocketService.registerHandler(WebSocket.Messages.REQUEST_RECEIVED, handler);
    }

    private void setupMessageReceivedHandler() {
        this.messageReceivedHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                JSONObject messageObject = (JSONObject)args[0];
                onMessageReceived(messageObject);
            }
        };

        this.webSocketService.registerHandler(WebSocket.Messages.MESSAGE_RECEIVED, messageReceivedHandler);
    }

    private void setupGetNotificationsHandlers() {
        final WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                JSONObject result = (JSONObject)args[0];
                onGetNotificationsSuccess(result);
            }
        };

        final WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String error = args[0].toString();
                Log.i(TAG, error);
            }
        };

        this.webSocketService.registerCallbacks(WebSocket.Messages.GET_NOTIFICATIONS, successHandler, failureHandler);
    }

    private void setupUnfriendedHandler() {
        WebSocketEventHandler handler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String issuerName = args[0].toString();
                onUnfriended(issuerName);
            }
        };

        this.webSocketService.registerHandler(WebSocket.Messages.UNFRIENDED, handler);
    }

    private void setupBlacklistedHandler() {
        WebSocketEventHandler handler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                JSONObject blacklistObject = (JSONObject)args[0];
                onBlacklisted(blacklistObject);
            }
        };

        this.webSocketService.registerHandler(WebSocket.Messages.BLACKLISTED, handler);
    }

    private void setupBlockedHandler() {
        WebSocketEventHandler handler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                onBlocked();
            }
        };

        this.webSocketService.registerHandler(WebSocket.Messages.BLOCKED, handler);
    }

    private void setupRequestAcceptedHandler() {
        WebSocketEventHandler handler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String accepterName = args[0].toString();
                onRequestAccepted(accepterName);
            }
        };

        this.webSocketService.registerHandler(WebSocket.Messages.REQUEST_ACCEPTED, handler);
    }


    private void onRequestReceived(String senderName) {
        Intent intent = Application.getMainActivityIntent(this);
        intent.setAction(MainActivity.ACTION_SHOW_REQUESTS);

        this.showNotification(
            getString(R.string.request_received_notification_title),
            getString(R.string.request_received_notification_body, senderName),
            intent,
            REQUEST_RECEIVED_NOTIFICATION_ID);
    }

    private void onMessageReceived(JSONObject messageObject) {
        PowerManager powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
        Boolean isScreenOn = powerManager.isScreenOn();
        String senderName, text;

        try {
            int senderID = messageObject.getInt(MessageAttributes.SENDER_USER_ID);

            // Message is visible in the chat activity
            if (isScreenOn && ChatActivity.isVisible() && (senderID == ChatActivity.getCurrentTarget().getID()))
                return;

            senderName = messageObject.getString(MessageAttributes.SENDER_USER_NAME);
            text = messageObject.getString(MessageAttributes.TEXT);
        } catch (JSONException e) {
            return;
        }

        Intent intent = Application.getMainActivityIntent(this);
        intent.setAction(MainActivity.ACTION_SHOW_FRIENDS);

        this.showNotification(
            getString(R.string.message_received_notification_title),
            String.format(getString(R.string.message_received_notification_body), senderName, text),
            intent,
            MESSAGE_RECEIVED_NOTIFICATION_ID
        );
    }

    private void onGetNotificationsSuccess(JSONObject result) {
        int newMessages = 0,
            newRequests = 0,
            newFriends = 0;

        try {
            newMessages = result.getInt(NotificationAttributes.NEW_MESSAGES);
            newRequests = result.getInt(NotificationAttributes.NEW_REQUESTS);
            newFriends = result.getInt(NotificationAttributes.NEW_FRIENDS);
        } catch (JSONException ex) { }

        if ((newMessages == 0) && (newRequests == 0) && (newFriends == 0))
            return;

        this.showNotification(
            getString(R.string.notification_title),
            String.format(getString(R.string.notification_body), newRequests, newMessages, newFriends),
            Application.getMainActivityIntent(this),
            SUMMARY_NOTIFICATION_ID
        );
    }

    private void onUnfriended(String issuerName) {
        Intent intent = Application.getMainActivityIntent(this);
        intent.setAction(MainActivity.ACTION_SHOW_FRIENDS);

        this.showNotification(
            getString(R.string.unfriended_notification_title),
            getString(R.string.unfriended_notification_body, issuerName),
            intent,
            UNFRIENDED_NOTIFICATION_ID
        );
    }

    private void onBlacklisted(JSONObject blacklistObject) {
        String issuerName, reason;

        try {
            issuerName = blacklistObject.getString(BlacklistAttributes.ISSUER_NAME);
            reason = blacklistObject.getString(BlacklistAttributes.REASON);
        } catch (JSONException e) {
            return;
        }

        this.showNotification(
            getString(R.string.blacklisted_notification_title),
            String.format(getString(R.string.blacklisted_notification_body), issuerName, reason),
            Application.getMainActivityIntent(this),
            BLACKLISTED_NOTIFICATION_ID
        );
    }

    private void onBlocked() {
        boolean appVisible =
            MainActivity.isVisible() || ChatActivity.isVisible() ||
            EditProfileDataActivity.isVisible() || EditSettingsActivity.isVisible() ||
            SelectPictureActivity.isVisible();

        if (LoginService.isAvailable())
            LoginService.getInstance().logout();
        else
            LoginService.forceLogout(this);

        if (!appVisible) {
            this.showNotification(
                getString(R.string.blocked_notification_title),
                getString(R.string.blocked_notification_body),
                Application.getStartMenuActivityIntent(this),
                BLOCKED_NOTIFICATION_ID
            );
        } else {
            Handler handler = new Handler(this.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(NotificationService.this);

                    builder.setTitle(getString(R.string.blocked_notification_title));
                    builder.setMessage(getString(R.string.blocked_notification_body));

                    builder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = Application.getStartMenuActivityIntent(NotificationService.this);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    });

                    Dialog dialog = builder.create();
                    dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                    dialog.show();
                }
            });
        }
    }

    private void onRequestAccepted(String accepterName) {
        Intent intent = Application.getMainActivityIntent(this);
        intent.setAction(MainActivity.ACTION_SHOW_FRIENDS);

        this.showNotification(
                getString(R.string.request_accepted_notification_title),
                getString(R.string.request_accepted_notification_body, accepterName),
                intent,
                REQUEST_ACCEPTED_NOTIFICATION_ID
        );
    }


    private void showNotification(String title, String body, Intent intent, int id) {
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.pickme)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(soundUri);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(id, builder.build());
    }
}
