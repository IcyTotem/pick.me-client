package org.pickme.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import org.pickme.entities.Friend;
import org.pickme.entities.Message;
import org.pickme.utils.Globals;
import org.pickme.entities.attributes.MessageAttributes;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MessageHistoryService extends Service {
    private static final String TAG  = MessageHistoryService.class.getSimpleName();

    private static MessageHistoryService instance;

    private WebSocketService webSocketService;
    private WebSocketEventHandler messageReceivedHandler, allMessagesReadByHandler;

    public static MessageHistoryService getInstance() {
        return instance;
    }

    public static boolean isAvailable() {
        return (instance != null);
    }


    public MessageHistoryService() {
        MessageHistoryService.instance = this;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if ((this.webSocketService = WebSocketService.getInstance()) != null)
            this.setupWebSocketHandlers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (this.webSocketService != null) {
            webSocketService.unregisterHandler(WebSocket.Messages.MESSAGE_RECEIVED, messageReceivedHandler);
            webSocketService.unregisterHandler(WebSocket.Messages.ALL_MESSAGES_READ_BY, allMessagesReadByHandler);
        }

        MessageHistoryService.instance = null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!webSocketService.hasHandler(WebSocket.Messages.MESSAGE_RECEIVED) ||
            !webSocketService.hasHandler(WebSocket.Messages.ALL_MESSAGES_READ_BY))
            this.setupWebSocketHandlers();

        return START_STICKY;
    }

    private void setupWebSocketHandlers() {
        this.messageReceivedHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                JSONObject messageObject = (JSONObject)args[0];
                onMessageReceived(messageObject);
            }
        };

        this.allMessagesReadByHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                Integer targetUserID = Integer.parseInt(args[0].toString());
                onAllMessagesReadBy(targetUserID);
            }
        };

        webSocketService.registerHandler(WebSocket.Messages.MESSAGE_RECEIVED, messageReceivedHandler);
        webSocketService.registerHandler(WebSocket.Messages.ALL_MESSAGES_READ_BY, allMessagesReadByHandler);
    }


    public void addMyMessageToHistory(Message message, int targetUserID) {
        this.save(message, targetUserID);
    }

    public List<Message> retrieveHistory(int targetUserID) {
        return this.load(targetUserID);
    }

    public void overrideHistory(List<Message> messages, int targetUserID) {
        this.save(messages, targetUserID, false);
    }

    public void addToHistory(List<Message> messages, int targetUserID) {
        this.save(messages, targetUserID, true);
    }


    private void onMessageReceived(JSONObject messageObject) {
        Message message = new Message();
        int senderID = 0;

        try {
            senderID = messageObject.getInt(MessageAttributes.SENDER_USER_ID);
            message.setID(messageObject.getInt(MessageAttributes.ID));
            message.setSendDate(messageObject.getString(MessageAttributes.SENT_DATE));
            message.setText(messageObject.getString(MessageAttributes.TEXT));
        } catch (JSONException e) {
            return;
        }

        Friend dummySender = new Friend();
        dummySender.setID(senderID);

        message.setSender(dummySender);

        this.save(message);
    }

    private void onAllMessagesReadBy(int targetUserID) {
        List<Message> allMessages = retrieveHistory(targetUserID);

        for (Message message : allMessages)
            message.setRead(true);

        overrideHistory(allMessages, targetUserID);
    }


    private void save(Message message) {
        this.save(message, message.getSender().getID());
    }

    private void save(Message message, int messageHistoryID) {
        File historyFile = this.getMessageHistoryFile(messageHistoryID);
        try {
            DataOutputStream stream = new DataOutputStream(new FileOutputStream(historyFile, historyFile.exists()));

            write(message, stream);

            stream.flush();
            stream.close();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    private void save(List<Message> messages, int messageHistoryID, boolean append) {
        File historyFile = this.getMessageHistoryFile(messageHistoryID);
        try {
            DataOutputStream stream = new DataOutputStream(new FileOutputStream(historyFile, append));

            for (Message message : messages)
                write(message, stream);

            stream.flush();
            stream.close();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    private List<Message> load(int messageHistoryID) {
        File historyFile = this.getMessageHistoryFile(messageHistoryID);

        if (!historyFile.exists())
            return new ArrayList<Message>();

        List<Message> result = new ArrayList<Message>();

        try {
            DataInputStream stream = new DataInputStream(new FileInputStream(historyFile));
            while (stream.available() > 0)
                result.add(read(stream));
            stream.close();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return result;
    }

    private File getMessageHistoryFile(int messageHistoryID) {
        String historyFileName =
            String.format("%s%s",
                String.format(Globals.MESSAGE_HISTORY_FILE_NAME_FORMAT, messageHistoryID),
                Globals.MESSAGE_HISTORY_FILE_EXTENSION);
        return new File(getFilesDir(), historyFileName);
    }


    private static void write(Message message, DataOutputStream stream) throws IOException {
        String sendTime = Globals.DATE_TIME_FORMAT.format(message.getSendDate().getTime());
        String text = message.getText();

        byte[] sendTimeBytes = sendTime.getBytes();
        byte[] textBytes = text.getBytes();

        stream.writeInt(message.getID());
        stream.writeBoolean(message.isMine());
        stream.writeBoolean(message.isRead());
        stream.writeInt(sendTimeBytes.length);
        stream.write(sendTimeBytes);
        stream.writeInt(textBytes.length);
        stream.write(textBytes);
    }

    private static Message read(DataInputStream stream) throws IOException {
        int id = stream.readInt();
        boolean mine = stream.readBoolean();
        boolean read = stream.readBoolean();

        int sendTimeBytesLength = stream.readInt();
        byte[] sendTimeBytes = new byte[sendTimeBytesLength];

        if (stream.read(sendTimeBytes) != sendTimeBytesLength)
            throw new IOException("Wrong number of bytes for SendTime!");

        int textBytesLength = stream.readInt();
        byte[] textBytes = new byte[textBytesLength];

        if (stream.read(textBytes) != textBytesLength)
            throw new IOException("Wrong number of bytes for Text!");

        String sendTime = new String(sendTimeBytes);
        String text = new String(textBytes);

        Message message = new Message();
        message.setID(id);
        message.setMine(mine);
        message.setRead(read);
        message.setSendDate(sendTime);
        message.setText(text);

        return message;
    }
}
