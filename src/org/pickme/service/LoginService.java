package org.pickme.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.pickme.R;
import org.pickme.utils.Globals;
import org.pickme.entities.attributes.UserAttributes;

import java.io.FileOutputStream;
import java.io.IOException;

public class LoginService extends Service {
    public static final String LOGIN_AUTOMATICALLY = "login-automatically";

    private static LoginService instance;
    private static Runnable readyHandler;

    private Credentials credentials;
    private WebSocketService webSocketService;
    private Runnable loginSuccessHandler, loginFailureHandler;

    private String lastError;
    private boolean logged;

    public String getLastError() {
        return lastError;
    }

    private void setLastError(String lastError) {
        this.lastError = lastError;
    }

    public boolean isLogged() {
        return logged;
    }

    private void setLogged(boolean logged) {
        this.logged = logged;
    }

    public static LoginService getInstance() {
        return instance;
    }

    public static boolean isAvailable() {
        return (instance != null);
    }


    public LoginService() {
        this.credentials = new Credentials();
        LoginService.instance = this;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if ((this.webSocketService = WebSocketService.getInstance()) != null)
            this.setupWebSocketHandlers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (this.webSocketService != null)
            this.webSocketService.unregisterCallbacks(WebSocket.Messages.LOGIN);

        LoginService.instance = null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (readyHandler != null)
            readyHandler.run();

        // I don't know why, but sometimes, callbacks disappear: even if this service is NOT destroyed,
        // and unregisterCallbacks is NEVER called, and no other service is EVER recreated
        if (!webSocketService.hasCallbacks(WebSocket.Messages.LOGIN))
            this.setupWebSocketHandlers();

        if (webSocketService.isConnected() && intent.getBooleanExtra(LOGIN_AUTOMATICALLY, false))
            this.loginAutomatically();

        return START_STICKY;
    }


    public void loginAutomatically() {
        if (this.isLogged()) {
            if (loginSuccessHandler != null)
                loginFailureHandler.run();
            return;
        }

        SharedPreferences sharedPrefs = UserAttributes.get(this);
        String secret = sharedPrefs.getString(UserAttributes.LOGIN_SECRET, null);

        if (secret == null) {
            this.setLastError(getResources().getString(R.string.error_no_login_secret));

            if (loginFailureHandler != null)
                loginFailureHandler.run();

            return;
        }

        TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);

        credentials.setDeviceID(telephonyManager.getDeviceId());
        credentials.setSecret(secret);
        credentials.setName(null);
        credentials.setPassword(null);

        WebSocket webSocket = webSocketService.getWebSocket();
        webSocket.emit(WebSocket.Messages.LOGIN, credentials);
    }

    public void loginManually(String username, String password) {
        if (this.isLogged()) {
            if (loginSuccessHandler != null)
                loginFailureHandler.run();
            return;
        }

        credentials.setName(username);
        credentials.setPassword(password);
        credentials.setDeviceID(null);
        credentials.setSecret(null);

        WebSocket webSocket = webSocketService.getWebSocket();
        webSocket.emit(WebSocket.Messages.LOGIN, credentials);
    }

    public void logout() {
        clearLocalData(this);

        WebSocket webSocket = webSocketService.getWebSocket();
        webSocket.emit(WebSocket.Messages.LOGOUT);
        setLogged(false);

        stopSessionServices();
    }

    public void quietLogout() {
        stopSessionServices();
    }
    
    public void runAfterLogin(Runnable runnable) {
        this.loginSuccessHandler = runnable;
    }

    public void runOnLoginFailure(Runnable runnable) {
        this.loginFailureHandler = runnable;
    }


    public static void forceLogout(Context context) {
        clearLocalData(context);
        stopSessionServices();
    }

    public static void forceQuietLogout() {
        stopSessionServices();
    }

    private static void clearLocalData(Context context) {
        SharedPreferences.Editor editor = UserAttributes.edit(context);

        editor.putString(UserAttributes.LOGIN_SECRET, null);
        editor.putString(UserAttributes.NAME, null);
        editor.putString(UserAttributes.BIRTHDAY, null);
        editor.putString(UserAttributes.GENDER, null);
        editor.putString(UserAttributes.DESCRIPTION, null);
        editor.putString(UserAttributes.LOOKING_FOR_GENDER, null);
        editor.putString(UserAttributes.LOOKING_FOR_AGE_MIN, null);
        editor.putString(UserAttributes.LOOKING_FOR_AGE_MAX, null);
        editor.putString(UserAttributes.LOOKING_FOR_RANGE, null);
        editor.putString(UserAttributes.LATITUDE, null);
        editor.putString(UserAttributes.LONGITUDE, null);
        editor.putString(UserAttributes.REQUEST_REPOST_DELAY, null);
        editor.putString(UserAttributes.REQUEST_DEFAULT_MESSAGE, null);
        editor.putString(UserAttributes.COUNTRY, null);

        editor.apply();
    }

    public static void runWhenReady(Runnable runnable) {
        LoginService.readyHandler = runnable;
    }


    private void setupWebSocketHandlers() {
        final WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String error = args[0].toString();
                onLoginFailure(error);
            }
        };

        final WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                JSONObject userObject = (JSONObject)args[0];
                onLoginSuccess(userObject);
            }
        };

        this.webSocketService.registerCallbacks(WebSocket.Messages.LOGIN, successHandler, failureHandler);
    }

    private void onLoginFailure(String error) {
        this.setLastError(error);
        this.setLogged(false);

        clearLocalData(LoginService.this);

        if (loginFailureHandler != null)
            loginFailureHandler.run();
    }

    private void onLoginSuccess(JSONObject userObject) {
        TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();

        try {
            String receivedDeviceID = userObject.getString(UserAttributes.DEVICE_ID);
            if (!deviceID.equals(receivedDeviceID)) {
                onLoginFailure(getResources().getString(R.string.error_device_id_mismatch));
                return;
            }
        } catch (JSONException e) {
            onLoginFailure(getResources().getString(R.string.error_no_login_device_id));
            return;
        }

        this.save(userObject);
        this.setLogged(true);
        this.startSessionServices();

        if (loginSuccessHandler != null)
            loginSuccessHandler.run();
    }

    private void save(JSONObject user) {
        SharedPreferences.Editor editor = UserAttributes.edit(this);

        editUserAttribute(editor, user, UserAttributes.LOGIN_SECRET);
        editUserAttribute(editor, user, UserAttributes.NAME);
        editUserAttribute(editor, user, UserAttributes.BIRTHDAY);
        editUserAttribute(editor, user, UserAttributes.GENDER);
        editUserAttribute(editor, user, UserAttributes.DESCRIPTION);
        editUserAttribute(editor, user, UserAttributes.LOOKING_FOR_GENDER);
        editUserAttribute(editor, user, UserAttributes.LOOKING_FOR_AGE_MIN);
        editUserAttribute(editor, user, UserAttributes.LOOKING_FOR_AGE_MAX);
        editUserAttribute(editor, user, UserAttributes.LOOKING_FOR_RANGE);
        editUserAttribute(editor, user, UserAttributes.LATITUDE);
        editUserAttribute(editor, user, UserAttributes.LONGITUDE);
        editUserAttribute(editor, user, UserAttributes.REQUEST_REPOST_DELAY);
        editUserAttribute(editor, user, UserAttributes.REQUEST_DEFAULT_MESSAGE);
        editUserAttribute(editor, user, UserAttributes.COUNTRY);

        editor.apply();

        try {
            String base64Picture = user.getString(UserAttributes.PICTURE);
            this.savePicture(base64Picture);
        } catch (JSONException e) { }
    }

    private void editUserAttribute(SharedPreferences.Editor editor, JSONObject user, String attribute) {
        try {
            editor.putString(attribute, user.getString(attribute));
        } catch (JSONException e) { }
    }

    private void savePicture(String base64Data) {
        try {
            FileOutputStream outputStream = openFileOutput(Globals.MY_PICTURE_B64_FILE_NAME, MODE_PRIVATE);
            outputStream.write(base64Data.getBytes());
            outputStream.close();
        } catch (IOException e) { }

        byte[] rawPictureData = Base64.decode(base64Data, Base64.DEFAULT);

        try {
            FileOutputStream outputStream = openFileOutput(Globals.MY_PICTURE_FILE_NAME, MODE_PRIVATE);
            outputStream.write(rawPictureData);
            outputStream.close();
        } catch (IOException e) { }
    }


    private void startSessionServices() {
        startService(new Intent(getApplicationContext(), GeolocationService.class));
        startService(new Intent(getApplicationContext(), PictureLoadingService.class));
        startService(new Intent(getApplicationContext(), NotificationService.class));
        startService(new Intent(getApplicationContext(), MessageHistoryService.class));
    }

    private static void stopSessionServices() {
        if (GeolocationService.isAvailable())
            GeolocationService.getInstance().stopSelf();

        if (MessageHistoryService.isAvailable())
            MessageHistoryService.getInstance().stopSelf();

        if (PictureLoadingService.isAvailable())
            PictureLoadingService.getInstance().stopSelf();

        if (NotificationService.isAvailable())
            NotificationService.getInstance().stopSelf();
    }


    private static class Credentials {
        private String deviceID, secret, name, password;

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public String getSecret() {
            return secret;
        }

        public void setSecret(String secret) {
            this.secret = secret;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
