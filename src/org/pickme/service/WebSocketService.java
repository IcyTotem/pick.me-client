package org.pickme.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;
import org.json.JSONObject;
import org.pickme.R;
import org.pickme.app.Application;
import org.pickme.app.MainActivity;
import org.pickme.utils.ToastHelper;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebSocketService extends Service {
    private static final int SERVICE_NOTIFICATION_ID = 42;
    private static final String SERVER_ENDPOINT = "http://80.85.85.247:3000";

    private static WebSocketService instance;
    private static Runnable readyHandler;

    private SocketIO socket;
    private WebSocket socketWrapper;
    private boolean initialized;
    private boolean foreground;

    private Thread connectionThread;
    private IOCallback ioCallback;

    private Runnable connectHandler, disconnectHandler;
    private Map<String, List<WebSocketEventHandler>> eventHandlers;

    private List<PendingEvent> pendingEvents;


    public static WebSocketService getInstance() {
        return instance;
    }

    public boolean isConnected() {
        return socket.isConnected();
    }

    public boolean isInitialized() {
        return initialized;
    }

    public WebSocket getWebSocket() {
        return socketWrapper;
    }


    public WebSocketService() {
        this.ioCallback = new CallbackStub();
        this.eventHandlers = new HashMap<String, List<WebSocketEventHandler>>();
        this.pendingEvents = new ArrayList<PendingEvent>();

        WebSocketService.instance = this;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        this.initialized = false;
        this.foreground = false;

        try {
            this.socket = new SocketIO(SERVER_ENDPOINT);
            this.initialized = true;
        } catch (MalformedURLException e) {
            this.initialized = false;
            return;
        }

        this.initWrappers();
        this.connectionThread.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        this.socket.disconnect();
        WebSocketService.instance = null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (readyHandler != null)
            readyHandler.run();

        return START_STICKY;
    }

    private void initWrappers() {
        this.socketWrapper = new WebSocket() {
            @Override
            public void emit(final String event, final Object... args) {
                if (socket.isConnected())
                    socket.emit(event, args);
                else {
                    // The socket has been disconneted, but the reconnect task is async. Therefore, we add the event to
                    // be emitted to a list of pending events that will be fired when onConnect is called in the CallbackStub
                    PendingEvent e = new PendingEvent();
                    e.setEvent(event);
                    e.setArgs(args);

                    pendingEvents.add(e);

                    // Then run the reconnect task (which is asynchronous anyway)
                    try {
                        socket = new SocketIO(SERVER_ENDPOINT);
                        socket.connect(ioCallback);
                    } catch (MalformedURLException mue) { }
                }
            }

            @Override
            public void disconnect() {
                socket.disconnect();
            }
        };

        this.connectionThread = new Thread(new Runnable() {
            @Override
            public void run() {
                socket.connect(ioCallback);
            }
        });
    }

    private void requireForeground() {
        if (foreground)
            return;

        NotificationCompat.Builder builder =
            new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.pickme)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.web_socket_service_foreground_description));

        Intent intent = Application.getStartMenuActivityIntent(this);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        Notification notification = builder.build();

        this.startForeground(SERVICE_NOTIFICATION_ID, notification);
        this.foreground = true;
    }


    public void retryConnect() {
        if (this.isConnected())
            return;

        if (connectionThread.isAlive())
            throw new IllegalStateException("Still trying to connect.");

        this.connectionThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new SocketIO(SERVER_ENDPOINT);
                    socket.connect(ioCallback);
                } catch (MalformedURLException mue) { }
            }
        });

        this.connectionThread.start();
    }


    public static void runWhenReady(Runnable readyHandler) {
        WebSocketService.readyHandler = readyHandler;
    }

    public void runOnConnect(Runnable connectHandler) {
        this.connectHandler = connectHandler;
    }

    public void runOnDisconnect(Runnable disconnectHandler) {
        this.disconnectHandler = disconnectHandler;
    }

    public void runWhenConnected(Runnable runnable) {
        if (this.isConnected() && (runnable != null))
            (new Thread(runnable)).start();
        else
            this.runOnConnect(runnable);
    }


    /**
     * Registers an event handler so that whenever the underlying websocket received the related event, its
     * corresponding handlers are called in response.
     * @param event The event name must match event names emitted from the server.
     * @param handler A non-null handler, which is basically a glorified Runnable that also takes as an Object
     *        array whichever parameters is passed by the server. Such Objects may be plain strings,
     *        JSONObject or JSONArray.
     */
    public void registerHandler(String event, WebSocketEventHandler handler) {
        List<WebSocketEventHandler> handlersList;

        if (eventHandlers.containsKey(event))
            handlersList = eventHandlers.get(event);
        else
            eventHandlers.put(event, handlersList = new ArrayList<WebSocketEventHandler>());

        handlersList.add(handler);
    }

    public void unregisterHandler(String event, WebSocketEventHandler handler) {
        if (!eventHandlers.containsKey(event))
            return;

        List<WebSocketEventHandler> handlersList = eventHandlers.get(event);
        handlersList.remove(handler);
    }

    public void unregisterHandlers(String event) {
        if (!eventHandlers.containsKey(event))
            return;

        List<WebSocketEventHandler> handlersList = eventHandlers.get(event);
        handlersList.clear();

        eventHandlers.remove(event);
    }

    public void unregisterAllHandlers() {
        eventHandlers.clear();
    }

    /**
     * Registers two event handlers that represent the success and failure of a given event.
     * Invoking this method is the same as invoking:
     *     registerHandler(emittedEvent + " success", successCallback);
     *     registerHandler(emittedEvent + " failure", failureCallback);
     * @param emittedEvent The name of the event emitted by this very client whose callback events must be handled.
     *        If an event (which has the meaning of command if issued by the client) is received by the server, processed
     *        and successfully completed, the server will reply back with "{emittedCommand} success". Otherwise, the
     *        suffix will be "failure".
     * @param successCallback A non-null handler to be invoked in case of success.
     * @param failureCallback A non-null handler to be invoked in case of failure.
     */
    public void registerCallbacks(String emittedEvent, WebSocketEventHandler successCallback, WebSocketEventHandler failureCallback) {
        this.registerHandler(emittedEvent + " success", successCallback);
        this.registerHandler(emittedEvent + " failure", failureCallback);
    }

    public void unregisterCallbacks(String emittedEvent) {
        this.unregisterHandlers(emittedEvent + " success");
        this.unregisterHandlers(emittedEvent + " failure");
    }

    public boolean hasCallbacks(String emittedEvent) {
        return eventHandlers.containsKey(emittedEvent + " success") &&
               eventHandlers.containsKey(emittedEvent + " failure");
    }

    public boolean hasHandler(String emittedEvent) {
        return eventHandlers.containsKey(emittedEvent);
    }

    public boolean hasHandler(String emittedEvent, WebSocketEventHandler handler) {
        if (handler == null)
            return false;

        List<WebSocketEventHandler> registeredHandlers = eventHandlers.get(emittedEvent);

        for (WebSocketEventHandler registeredHandler : registeredHandlers)
            if (handler.equals(registeredHandler))
                return true;

        return false;
    }


    private class CallbackStub implements IOCallback {
        @Override
        public void onDisconnect() {
            if (WebSocketService.this.disconnectHandler != null)
                WebSocketService.this.disconnectHandler.run();
        }

        @Override
        public void onConnect() {
            if (WebSocketService.this.connectHandler != null)
                WebSocketService.this.connectHandler.run();

            WebSocketService.this.requireForeground();

            if (pendingEvents.size() > 0) {
                for (PendingEvent e : pendingEvents)
                    socket.emit(e.getEvent(), e.getArgs());
                pendingEvents.clear();
            }
        }

        @Override
        public void onMessage(String data, IOAcknowledge ack) { }

        @Override
        public void onMessage(JSONObject json, IOAcknowledge ack) { }

        @Override
        public void on(String event, IOAcknowledge ack, Object... args) {
            if (!WebSocketService.this.eventHandlers.containsKey(event)) {
                Log.i("WEBSOCKET-SERVICE", event + " unhandled");
                return;
            }

            List<WebSocketEventHandler> handlers = WebSocketService.this.eventHandlers.get(event);

            for (WebSocketEventHandler handler : handlers)
                handler.onEvent(args);
        }

        @Override
        public void onError(SocketIOException socketIOException) {
            Log.e("SOCKET-IO", socketIOException.getMessage(), socketIOException);
            ToastHelper.showToastInUiThread(WebSocketService.this, R.string.error_network);
        }
    }

    private static class PendingEvent {
        private String event;
        private Object[] args;

        public String getEvent() {
            return event;
        }

        public void setEvent(String event) {
            this.event = event;
        }

        public Object[] getArgs() {
            return args;
        }

        public void setArgs(Object[] args) {
            this.args = args;
        }
    }
}
