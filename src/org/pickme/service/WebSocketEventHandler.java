package org.pickme.service;

public interface WebSocketEventHandler {
    void onEvent(Object... args);
}
