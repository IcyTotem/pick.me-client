package org.pickme.service;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;
import org.pickme.R;
import org.pickme.utils.Globals;
import org.pickme.utils.UserSettings;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GeolocationService extends Service implements LocationListener {
    private static final String TAG = GeolocationService.class.getSimpleName();

    private static final int TWO_MINUTES = 1000 * 60 * 2;
    private static GeolocationService instance;

    private LocationManager locationManager;
    private Location currentLocation;
    private String currentProvider;

    private WebSocketService webSocketService;
    private WebSocket webSocket;

    private boolean gpsEnabled, networkEnabled;
    private boolean lowBattery, veryLowBattery;


    public static GeolocationService getInstance() {
        return instance;
    }

    public static boolean isAvailable() {
        return (instance != null);
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }


    public GeolocationService() {
        GeolocationService.instance = this;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        this.webSocketService = WebSocketService.getInstance();

        if (webSocketService == null)
            throw new NullPointerException("No WebSocketService available!");

        this.webSocket = this.webSocketService.getWebSocket();

        this.locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        this.gpsEnabled = this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        this.networkEnabled = this.locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        this.lowBattery = false;
        this.veryLowBattery = false;

        this.setupWebSocketHandlers();

        if (gpsEnabled || networkEnabled)
            this.registerForLocationUpdate();
        else
            this.activateNoProviderDeltaProtocol();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        this.unregisterForLocationUpdate();
        this.webSocketService.unregisterCallbacks(WebSocket.Messages.UPDATE_POSITION);

        GeolocationService.instance = null;
    }

    private void setupWebSocketHandlers() {
        WebSocketEventHandler successHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                Log.i(TAG, "Location updated successfully!");
            }
        };

        WebSocketEventHandler failureHandler = new WebSocketEventHandler() {
            @Override
            public void onEvent(Object... args) {
                String error = args[0].toString();
                Log.e(TAG, "Location update failed: " + error);
            }
        };

        this.webSocketService.registerCallbacks(WebSocket.Messages.UPDATE_POSITION, successHandler, failureHandler);
    }


    private void registerForLocationUpdate(String provider) {
        SharedPreferences sharedPrefs = UserSettings.get(this);

        if (lowBattery) {
            // If the battery is low, choose the network provider, hands down
            provider = LocationManager.NETWORK_PROVIDER;
        } else {
            // Otherwise, take the supplied provider, unless it is null: in that case, load the user preferred
            // provider from shared preferences
            if (provider == null)
                provider = sharedPrefs.getString(UserSettings.LOCATION_PROVIDER, UserSettings.DEFAULT_LOCATION_PROVIDER);

            // But if the chosen provider is not enabled, fall back to the other one
            if (!locationManager.isProviderEnabled(provider))
                provider = getAlternativeProvider(provider);
        }

        // Finally, if even the definite provider is not enabled, start the Delta (C) protocol
        if (!locationManager.isProviderEnabled(provider)) {
            this.activateNoProviderDeltaProtocol();
            return;
        }

        this.currentProvider = provider;

        long minTime = sharedPrefs.getLong(UserSettings.LOCATION_UPDATE_PERIOD, UserSettings.DEFAULT_LOCATION_UPDATE_PERIOD);
        long minDistance = sharedPrefs.getLong(UserSettings.LOCATION_UPDATE_DISTANCE, UserSettings.DEFAULT_LOCATION_UPDATE_DISTANCE);

        // If the battery is very low, increase the update period and distance dispatched to the provider
        if (veryLowBattery) {
            minTime = (long)(minTime * Globals.VERY_LOW_BATTERY_LOCATION_DAMP_FACTOR);
            minDistance = (long)(minDistance * Globals.VERY_LOW_BATTERY_LOCATION_DAMP_FACTOR);
        }

        locationManager.requestLocationUpdates(currentProvider, minTime, minDistance, this);

        Location location = locationManager.getLastKnownLocation(currentProvider);

        if (location != null)
            this.onLocationChanged(location);
    }

    private void registerForLocationUpdate() {
        registerForLocationUpdate(null);
    }

    private void unregisterForLocationUpdate() {
        this.locationManager.removeUpdates(this);
    }

    public void reloadUpdatePeriods() {
        this.unregisterForLocationUpdate();
        this.registerForLocationUpdate(currentProvider);
    }


    /**
     * This method will be called by the BatteryLevelReceiver on significant changes of the battery charge level.
     */
    public void onBatteryLevelShift(float batteryPercent) {
        this.lowBattery = (batteryPercent <= Globals.LOW_BATTERY_THRESHOLD);
        this.veryLowBattery = (batteryPercent <= Globals.VERY_LOW_BATTERY_THRESHOLD);

        this.unregisterForLocationUpdate();
        this.registerForLocationUpdate();
    }


    @Override
    public void onLocationChanged(Location location) {
        if ((location != null) && isBetterLocation(location)) {
            SimpleLocation sl = new SimpleLocation();
            sl.setLatitude(location.getLatitude());
            sl.setLongitude(location.getLongitude());

            this.currentLocation = location;
            this.webSocket.emit(WebSocket.Messages.UPDATE_POSITION, sl);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case LocationProvider.AVAILABLE:
                onProviderEnabled(provider);
                break;

            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                // Do nothing, just wait for it to return available
                break;

            case LocationProvider.OUT_OF_SERVICE:
                onProviderDisabled(provider);
                break;
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        this.refreshProviders();
    }

    @Override
    public void onProviderDisabled(String provider) {
        this.refreshProviders();

        // The provider this lister is attached to has been disabled, therefore we must cancel this handler
        this.unregisterForLocationUpdate();

        if (gpsEnabled)
            this.registerForLocationUpdate(LocationManager.GPS_PROVIDER);
        else if (networkEnabled)
            this.registerForLocationUpdate(LocationManager.NETWORK_PROVIDER);
        else
            this.activateNoProviderDeltaProtocol();
    }


    private void refreshProviders() {
        this.gpsEnabled = this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        this.networkEnabled = this.locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void activateNoProviderDeltaProtocol() {
        this.startProviderSearchThread();
        this.showNoProviderErrorDialog();
    }

    private void showNoProviderErrorDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final Context context = this;

        AlertDialog dialog =
            builder.setTitle(getResources().getString(R.string.error_title_no_provider_enabled))
                   .setMessage(getResources().getString(R.string.error_no_provider_enabled))
                   .setPositiveButton(context.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                           Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                           intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                           context.startActivity(intent);
                       }
                   })
                   .create();

        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.show();
    }

    private void startProviderSearchThread() {
        final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        final GeolocationService context = this;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (GeolocationService.getInstance() == null) {
                    executor.shutdownNow();
                    return;
                }

                if (context.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || context.locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    executor.shutdownNow();
                    context.registerForLocationUpdate();
                }
            }
        };

        SharedPreferences sharedPrefs = UserSettings.get(this);
        long minTime = sharedPrefs.getLong(UserSettings.LOCATION_UPDATE_PERIOD, UserSettings.DEFAULT_LOCATION_UPDATE_PERIOD);

        executor.scheduleAtFixedRate(runnable, 0, minTime, TimeUnit.MILLISECONDS);
    }


    private boolean isBetterLocation(Location newLocation) {
        if (currentLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = newLocation.getTime() - currentLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (newLocation.getAccuracy() - currentLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(newLocation.getProvider(),
                currentLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    private static boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private static String getAlternativeProvider(String provider) {
        if (provider.equals(LocationManager.GPS_PROVIDER))
            return LocationManager.NETWORK_PROVIDER;

        return LocationManager.GPS_PROVIDER;
    }


    private static class SimpleLocation {
        private double latitude, longitude;

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }
    }
}
