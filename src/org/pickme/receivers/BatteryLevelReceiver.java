package org.pickme.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import org.pickme.service.GeolocationService;

public class BatteryLevelReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context ctx, Intent intent) {
        GeolocationService geolocationService = GeolocationService.getInstance();

        if (geolocationService == null)
            return;

        int scale = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int level = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPercent = level / (float)scale;

        geolocationService.onBatteryLevelShift(batteryPercent);
    }
}
