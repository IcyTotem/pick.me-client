package org.pickme.entities.attributes;

public class MessageAttributes {
    public static final String ID = "ID";
    public static final String SENT_DATE = "SentDate";
    public static final String TEXT = "Text";
    public static final String SENDER_USER_ID = "SenderUserID";
    public static final String HASH_CODE = "HashCode";
    public static final String READ = "Read";
    public static final String SENDER_USER_NAME = "SenderUserName";
}
