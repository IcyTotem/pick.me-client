package org.pickme.entities.attributes;

public class FriendAttributes {
    public static final String UNREAD_MSG_SENDER_USER_ID = "SenderUserID";
    public static final String UNREAD_MSG_COUNT = "Count";
}
