package org.pickme.entities.attributes;

public class RequestingUserAttributes {
    public static final String REQUEST_ID = "RequestID";
    public static final String REQUEST_TEXT = "Text";
    public static final String REQUEST_ISSUE_DATE = "IssueDate";
}
