package org.pickme.entities.attributes;

public class NotificationAttributes {
    public static final String NEW_MESSAGES = "NewMessages";
    public static final String NEW_REQUESTS = "NewRequests";
    public static final String NEW_FRIENDS = "NewFriends";
}
