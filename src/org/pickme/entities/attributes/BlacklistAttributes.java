package org.pickme.entities.attributes;

public class BlacklistAttributes {
    public static final String ISSUER_NAME = "IssuerName";
    public static final String REASON = "Reason";
}
