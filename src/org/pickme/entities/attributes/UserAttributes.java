package org.pickme.entities.attributes;

import android.content.Context;
import android.content.SharedPreferences;

public class UserAttributes {
    public static final String ID = "ID";
    public static final String LOGIN_SECRET = "Secret";
    public static final String NAME = "Name";
    public static final String GENDER = "Gender";
    public static final String BIRTHDAY = "Birthday";
    public static final String COUNTRY = "Country";
    public static final String DESCRIPTION = "Description";
    public static final String LOOKING_FOR_GENDER = "LookingForGender";
    public static final String LOOKING_FOR_AGE_MIN = "LookingForAgeMin";
    public static final String LOOKING_FOR_AGE_MAX = "LookingForAgeMax";
    public static final String LOOKING_FOR_RANGE = "LookingForRange";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String REQUEST_REPOST_DELAY = "RequestRepostDelay";
    public static final String REQUEST_DEFAULT_MESSAGE = "RequestDefaultMessage";
    public static final String LAST_UPDATE = "LastUpdate";
    public static final String PICTURE = "Picture";
    public static final String DEVICE_ID = "DeviceID";
    public static final String FIRST_LOGIN = "FirstLogin";
    public static final String RANGE = "Distance"; // Only used for interpreting server queries
    public static final String LOOKING_FOR_COMPLETED = "LookingForCompleted"; // indicates whether search preferences were set at least once

    private static final String LOCAL_USER_ATTRIBUTES = "local-user-attributes";

    public static SharedPreferences get(Context context) {
        return context.getSharedPreferences(LOCAL_USER_ATTRIBUTES, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor edit(Context context) {
        return get(context).edit();
    }
}
