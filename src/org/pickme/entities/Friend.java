package org.pickme.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.util.Calendar;
import java.util.Comparator;

public class Friend extends User {
    private int unreadMessagesCount;

    public int getUnreadMessagesCount() {
        return unreadMessagesCount;
    }

    public void setUnreadMessagesCount(int unreadMessagesCount) {
        this.unreadMessagesCount = unreadMessagesCount;
    }


    public Friend() { }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(this.getUnreadMessagesCount());
    }

    public Friend(Parcel in) {
        super(in);
        this.setUnreadMessagesCount(in.readInt());
    }


    public static final Comparator<Friend> UNREAD_MESSAGES_COUNT_COMPARATOR = new Comparator<Friend>() {
        @Override
        public int compare(Friend friend1, Friend friend2) {
            Integer count1 = friend1.getUnreadMessagesCount();
            Integer count2 = friend2.getUnreadMessagesCount();
            return count2.compareTo(count1);
        }
    };

    public static final Parcelable.Creator<Friend> CREATOR = new Parcelable.Creator<Friend>() {
        public Friend createFromParcel(Parcel in) {
            return new Friend(in);
        }

        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };
}
