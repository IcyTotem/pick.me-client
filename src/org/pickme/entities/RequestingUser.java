package org.pickme.entities;

import android.os.Parcel;
import org.pickme.utils.Globals;

import java.io.File;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class RequestingUser extends User {
    private int requestID;
    private String requestText;
    private Calendar requestIssueDate;

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public String getRequestText() {
        return requestText;
    }

    public void setRequestText(String requestText) {
        this.requestText = requestText;
    }

    public Calendar getRequestIssueDate() {
        return requestIssueDate;
    }

    public void setRequestIssueDate(Calendar requestIssueDate) {
        this.requestIssueDate = requestIssueDate;
    }

    public void setRequestIssueDate(String issueDate) {
        try {
            Date date = Globals.DATE_FORMAT.parse(issueDate);

            this.requestIssueDate = Calendar.getInstance();
            this.requestIssueDate.setTime(date);
        } catch (ParseException e) { }
    }

    public boolean hasRequestIssueDate() {
        return (requestIssueDate != null);
    }


    public RequestingUser() { }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(this.getRequestID());
        out.writeString(this.getRequestText());
        out.writeSerializable(this.getRequestIssueDate());
    }

    public RequestingUser(Parcel in) {
        super(in);
        this.setRequestID(in.readInt());
        this.setRequestText(in.readString());
        this.setRequestIssueDate((Calendar)in.readSerializable());
    }
}
