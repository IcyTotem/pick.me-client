package org.pickme.entities;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import org.pickme.utils.Globals;
import org.pickme.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

public class User implements Parcelable {
    private static String NO_UPDATE_PERIOD = "never";

    private int id;
    private String name, gender, description;
    private Calendar birthday, lastUpdate;
    private File pictureFile;
    private int range;


    public User() { }


    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean hasDescription() {
        return (description != null) && !description.equals("") && !description.equals("null");
    }


    public Calendar getBirthday() {
        return birthday;
    }

    public void setBirthday(Calendar birthday) {
        this.birthday = birthday;
    }

    public void setBirthday(String birthday) {
        try {
            Date birthdayDate = Globals.DATE_FORMAT.parse(birthday);

            this.birthday = Calendar.getInstance();
            this.birthday.setTime(birthdayDate);
        } catch (ParseException e) { }
    }

    public int getAge() {
        if (birthday == null)
            return -1;
        else
            return Utils.computeAge(birthday);
    }

    public boolean hasBirthday() {
        return (birthday != null);
    }


    public Calendar getLastUpdate() {
        return lastUpdate;
    }

    public int getMinutesSinceLastUpdate() {
        if (lastUpdate == null) {
            return -1;
        } else {
            Calendar now = Calendar.getInstance();
            return Utils.differenceInMinutes(lastUpdate, now);
        }
    }

    public void setLastUpdate(Calendar lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        lastUpdate = lastUpdate.replace("T", " ");
        try {
            Date lastUpdateDate = Globals.DATE_TIME_FORMAT.parse(lastUpdate);

            this.lastUpdate = Calendar.getInstance();
            this.lastUpdate.setTime(lastUpdateDate);
        } catch (ParseException e) { }
    }

    public boolean hasLastUpdate() {
        return (lastUpdate != null);
    }

    public String getPeriodSinceLastUpdate() {
        if (!this.hasLastUpdate())
            return NO_UPDATE_PERIOD;

        int minutes = this.getMinutesSinceLastUpdate();

        if (minutes < 60)
            return (String.valueOf(minutes) + "m");

        int hours = minutes / 60;
        int remainingMinutes = minutes % 60;

        if (hours < 24)
            return (String.valueOf(hours) + "h, " + String.valueOf(remainingMinutes) + "m");

        int days = hours / 24;
        int remainingHours = hours % 24;

        return (String.valueOf(days) + "d, " + String.valueOf(remainingHours) + "h");
    }


    public File getPictureFile() {
        return pictureFile;
    }

    public void setPictureFile(File pictureFile) {
        this.pictureFile = pictureFile;
    }

    public void setPictureFile(Context context, String base64Picture) {
        File targetFile =
            new File(context.getFilesDir(),
                String.format(Globals.PROFILE_PICTURE_NAME_FORMAT, this.getID()) + Globals.PROFILE_PICTURE_EXTENSION);

        try {
            FileOutputStream stream = new FileOutputStream(targetFile);
            byte[] data = Base64.decode(base64Picture, Base64.DEFAULT);
            stream.write(data, 0, data.length);
            stream.close();

            this.pictureFile = targetFile;
        } catch (IOException ex) { }
    }

    public void setPictureFile(Context context) {
        this.pictureFile = new File(context.getFilesDir(), String.format(Globals.PROFILE_PICTURE_NAME_FORMAT, this.getID()) + Globals.PROFILE_PICTURE_EXTENSION);
    }

    public boolean hasPictureFile() {
        return (pictureFile != null) && pictureFile.exists();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(this.getID());
        out.writeString(this.getName());
        out.writeString(this.getGender());
        out.writeString(this.getDescription());
        out.writeSerializable(this.getBirthday());
        out.writeSerializable(this.getLastUpdate());
        out.writeInt(this.getRange());

        if (this.hasPictureFile())
            out.writeString(this.getPictureFile().getAbsolutePath());
        else
            out.writeString(null);
    }

    public User(Parcel in) {
        this.setID(in.readInt());
        this.setName(in.readString());
        this.setGender(in.readString());
        this.setDescription(in.readString());
        this.setBirthday((Calendar)in.readSerializable());
        this.setLastUpdate((Calendar)in.readSerializable());
        this.setRange(in.readInt());

        String pictureFilePath = in.readString();

        if (pictureFilePath != null)
            this.setPictureFile(new File(pictureFilePath));
    }


    public static final Comparator<User> RANGE_COMPARATOR = new Comparator<User>() {
        @Override
        public int compare(User user1, User user2) {
            Integer r1 = user1.getRange();
            Integer r2 = user2.getRange();
            return r1.compareTo(r2);
        }
    };

    public static final Comparator<User> LAST_UPDATE_COMPARATOR = new Comparator<User>() {
        @Override
        public int compare(User user1, User user2) {
            if (user1.hasLastUpdate() && user2.hasLastUpdate())
                return user2.getLastUpdate().compareTo(user1.getLastUpdate());
            if (user1.hasLastUpdate())
                return -1;
            if (user2.hasLastUpdate())
                return 1;
            return 0;
        }
    };

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
