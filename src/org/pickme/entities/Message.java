package org.pickme.entities;

import org.pickme.utils.Globals;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class Message {
    private int id;
    private boolean read, mine;
    private String text;
    private Calendar sendDate;
    private Friend sender;

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public Friend getSender() {
        return sender;
    }

    public void setSender(Friend sender) {
        this.sender = sender;

        if (sender != null)
            this.setMine(false);
    }

    public boolean isMine() {
        return mine;
    }

    public void setMine(boolean mine) {
        this.mine = mine;

        if (mine)
            this.setSender(null);
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Calendar getSendDate() {
        return sendDate;
    }

    public void setSendDate(Calendar sendDate) {
        this.sendDate = sendDate;
    }

    public void setSendDate(String sendDate) {
        try {
            Date date = Globals.DATE_TIME_FORMAT.parse(sendDate.replace("T", " "));
            this.sendDate = Calendar.getInstance();
            this.sendDate.setTime(date);
        } catch (ParseException e) { }
    }


    public Message() {
        this.mine = false;
        this.read = false;
        this.id = -1;
    }
}
