package io.socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class JSONConverter {
    private static Map<Class, List<Method>> typeGetters = new HashMap<Class, List<Method>>();
    private static List<Class> basicTypes;

    static {
        basicTypes = new ArrayList<Class>();
        basicTypes.add(Integer.class);
        basicTypes.add(Short.class);
        basicTypes.add(Long.class);
        basicTypes.add(Float.class);
        basicTypes.add(Double.class);
        basicTypes.add(String.class);
        basicTypes.add(Boolean.class);
        basicTypes.add(Character.class);
    };

    public static JSONObject toJSON(Object obj) {
        JSONObject result = new JSONObject();
        List<Method> getters = getGetters(obj);

        for (Method getter : getters) {
            String key = getGetterField(getter.getName());
            Object value = null;

            try {
                value = getter.invoke(obj);
            } catch (Exception e) {
                continue;
            }

            if (value == null)
                continue;

            try {
                if (isBasicType(value.getClass()))
                    result.put(key, value.toString());
                else
                    result.put(key, toJSON(value));
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return result;
    }

    public static JSONArray toJSONArray(Object... objects) {
        JSONArray result = new JSONArray();

        for (Object obj : objects)
            if (isBasicType(obj.getClass()))
                result.put(obj);
            else
                result.put(toJSON(obj));

        return result;
    }

    private static List<Method> getGetters(Object obj) {
        Class klass = obj.getClass();

        if (typeGetters.containsKey(klass))
            return typeGetters.get(klass);

        Method[] methods = obj.getClass().getMethods();
        List<Method> getters = new ArrayList<Method>();

        for (Method method : methods)
            if (method.getName().startsWith("get") && !method.getName().equals("getClass") && (method.getParameterTypes().length == 0))
                getters.add(method);

        typeGetters.put(klass, getters);
        return getters;
    }

    private static String getGetterField(String getterName) {
        if (getterName.length() > 3) {
            String fieldName = getterName.substring(3);

            if (fieldName.length() == 1)
                return fieldName.toLowerCase();
            else
                return Character.toLowerCase(fieldName.charAt(0)) + fieldName.substring(1);
        }
        return "default";
    }

    private static boolean isBasicType(Class klass) {
        for (Class basicType : basicTypes)
            if (klass.equals(basicType))
                return true;

        return false;
    }
}
